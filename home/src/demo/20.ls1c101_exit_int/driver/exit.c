#include"Main.h"

/**************************************************************
 * 外部中断只有32个,具体对应关系如下
 * GPIO0~GPIO7   对于外部中断 0~7
 * GPIO16~GPIO23 对于外部中断 8~15
 * GPIO32~GPIO39 对于外部中断 16~23
 * GPIO48~GPIO55 对于外部中断 24~31
 * ************************************************************/
 void gpio_irq_en(INT32U pin)
{
	INT32U exit_num;
	if(pin>=GPIO_PIN_MAX) 
		return;
	
	if(((pin/8)%2) != 0)          //这些管脚不是外部中断
		return;
	
	if(pin >= 32)
		PMU_GPIOA_OE  &=  ~(((INT32U)0x01) << pin);
	else
		PMU_GPIOB_OE  &=  ~(((INT32U)0x01) << pin);    
		
	exit_num = pin%16 + pin/16*8;    
	PMU_ExintEn |= (((INT32U)0x01) << exit_num);
	gpio_irq_clear(pin);                             //清除边沿模式的中断
	PMU_CmdSts 	|= (1<<25);                       //写1，外部中断唤醒1有效    
}
 
//设置IO中断模式
void gpio_set_irq_mode(INT32U pin, INT32U mode)
{
	INT32U exit_num;
	if(pin>=GPIO_PIN_MAX) 
		return;
	
	if(((pin/8)%2) != 0)          //这些管脚不是外部中断
		return;
		
	exit_num = pin%16 + pin/16*8;
	if(GPIO_IRQ_MODE_LEVEL == mode)
	{
		PMU_ExintEdge &= ~(((INT32U)0x01) << exit_num);     //电平模式
	}     
	else
	{
		PMU_ExintEdge |= (((INT32U)0x01) << exit_num);      //边沿模式
	}         
}

//设置IO中断极性
void gpio_set_irq_pol(INT32U pin, INT32U pol)
{
	INT32U exit_num;
	if(pin>=GPIO_PIN_MAX) 
		return;
	
	if(((pin/8)%2) != 0)          //这些管脚不是外部中断
		return;
		
	exit_num = pin%16 + pin/16*8;
	if(GPIO_IRQ_POL_HIGH == pol)
	{
		PMU_ExintPol &= ~(((INT32U)0x01) << exit_num);
	}     
	else
	{
		PMU_ExintPol |= (((INT32U)0x01) << exit_num);
	}         
}

//对电平模式无效
void gpio_irq_clear(INT32U pin)
{
	INT32U exit_num;
	if(pin>=GPIO_PIN_MAX) 
		return;
	
	if(((pin/8)%2) != 0)          //这些管脚不是外部中断
		return;
		
	exit_num = pin%16 + pin/16*8;
	PMU_ExintSrc |= (((INT32U)0x01) << exit_num);
}

void gpio_irq_clear_all(void)
{
	PMU_ExintSrc |= 0xffffffff; //写清中断标志
}

INT32U gpio_irq_get_label(void)
{
	return PMU_ExintSrc |= 0xffffffff;     //获取哪个GPIO中断触发
}

void gpio_exter_en(void)
{
	PMU_CmdSts 	  |= (1<<25);     //外部中断总开关
}


void gpio_exter_init(void)
{
	gpio_pin_remap(GPIO_PIN_38,GPIO_FUNC_GPIO);   //设置GPIO
	gpio_pin_remap(GPIO_PIN_4,GPIO_FUNC_GPIO);
	gpio_set_direction(GPIO_PIN_38,GPIO_INTPUT);  //设置为输入
	gpio_set_direction(GPIO_PIN_4,GPIO_INTPUT);
	
	//中断管脚使能
	gpio_irq_en(GPIO_PIN_38);
	gpio_irq_en(GPIO_PIN_4);
	
	//设置中断触发模式
	gpio_set_irq_mode(GPIO_PIN_38,GPIO_IRQ_MODE_EDGE);     //设置边沿触发模式
	gpio_set_irq_mode(GPIO_PIN_4,GPIO_IRQ_MODE_EDGE);
	
	//设置中断极性
	gpio_set_irq_pol(GPIO_PIN_38,GPIO_IRQ_POL_HIGH);       //设置上升岩触发
	gpio_set_irq_pol(GPIO_PIN_4,GPIO_IRQ_POL_LOW);        //设置下降岩触发
	
	//清除边沿模式中断
	gpio_irq_clear_all();
	//外部中断使能
	gpio_exter_en();                                       
}
