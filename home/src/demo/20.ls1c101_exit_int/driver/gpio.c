#include"Main.h"

/*	龙芯1c101管脚有4种配置模式
 * 	1. GPIO模式
 * 	2. 主功能模式
 * 	3. 第一复用模式
 * 	4. 第二复用模式
 * 	默认为GPIO模式,其中GPIO0,GPIO10,GPIO11,GPIO18,GPIO19,GPIO20,GPI021,GPIO40
 * 	GPIO41、GPIO42、GPIO43、GPIO59、GPIO58、GPIO60、GPIO63在芯片上,没有引出.
 */

//GPIO配置为输入
int gpio_read_pin(INT8U pin)
{   
	if(pin>=GPIO_PIN_MAX)
		return -1;
	PMU_GPIOBit(pin) = 0x00;
    return PMU_GPIOBit(pin);
}


/*
 *  GPIO输出
 *  pin:GPIO管脚
 *  mode: 输出电平高低 
 */
void gpio_write_pin(INT8U pin,INT8U mode)
{	
	if(pin>=GPIO_PIN_MAX)
		return;
		
	if(GPIO_HIGH == mode)
	{
		PMU_GPIOBit(pin) = 0x3;
	}
	else 
	{
		PMU_GPIOBit(pin) = 0x2;
	}
}

void gpio_set_direction(INT8U pin,INT8U mode)
{
	if(pin>=GPIO_PIN_MAX)
		return;
	
	if(pin >= 31)
	{
		if(mode == GPIO_INTPUT)
			PMU_GPIOA_I &= ~(1<<pin); 
		else
			PMU_GPIOA_O |= (1<<pin);
	}
	else
	{
		if(mode == GPIO_INTPUT)
			PMU_GPIOB_I &= ~(1<<pin); 
		else
			PMU_GPIOB_O |= (1<<pin); 
	}
}

//GPIO管脚复用
void gpio_pin_remap(INT32U pin,INT8U func)
{
	INT32U gpio_group = pin/16;
	INT32U gpio_offset = (pin%16)*2;
	
	if(pin>=GPIO_PIN_MAX)
		return;
		
	switch(gpio_group)
	{
		case 0:
			PMU_IOSEL0 &= ~(0x03<<gpio_offset);
			PMU_IOSEL0 |= (func<<gpio_offset);
		break;
		case 1:
			PMU_IOSEL1 &= ~(0x03<<gpio_offset);
			PMU_IOSEL1 |= (func<<gpio_offset);
		break;
		case 2:
			PMU_IOSEL2 &= ~(0x03<<gpio_offset);
			PMU_IOSEL2 |= (func<<gpio_offset);
		break;
		case 3:
			PMU_IOSEL3 &= ~(0x03<<gpio_offset);
			PMU_IOSEL3 |= (func<<gpio_offset);
		break;
		default:
		break;
	}
}






