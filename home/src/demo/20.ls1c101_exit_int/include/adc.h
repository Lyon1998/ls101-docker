#ifndef _ADC_H_
#define _ADC_H_

#define WriteKeyReg(LeftData,ReghtData)\
{\
	unsigned char GlobalIntFlag = IsGlobalIntOpen();\
 	DisableInt();\
	LeftData     = ReghtData;\
	if(GlobalIntFlag){\
		EnableInt();\
	}\
}

void 		detect_power(void);
INT32U		ls101_adc_mensure(volatile char channel);
void 		insert_sort(INT32U num[], int n);
#endif
