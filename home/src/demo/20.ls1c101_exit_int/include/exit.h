#ifndef _EXIT_H_
#define _EXIT_H_

#define GPIO_IRQ_POL_HIGH     0x01
#define GPIO_IRQ_POL_LOW      0x00

#define GPIO_IRQ_MODE_LEVEL   0x00
#define GPIO_IRQ_MODE_EDGE    0x01

#define EXIT_GPIO0    		  (1<<0)
#define EXIT_GPIO1            (1<<1)
#define EXIT_GPIO2            (1<<2)
#define EXIT_GPIO3            (1<<3)
#define EXIT_GPIO4            (1<<4)
#define EXIT_GPIO5            (1<<5)
#define EXIT_GPIO6            (1<<6)
#define EXIT_GPIO7            (1<<7)

#define EXIT_GPIO16           (1<<8)
#define EXIT_GPIO17           (1<<9)
#define EXIT_GPIO18           (1<<10)
#define EXIT_GPIO19           (1<<11)
#define EXIT_GPIO20           (1<<12)
#define EXIT_GPIO21           (1<<13)
#define EXIT_GPIO22           (1<<14)
#define EXIT_GPIO23           (1<<15)

#define EXIT_GPIO32           (1<<16)
#define EXIT_GPIO33           (1<<17)
#define EXIT_GPIO34           (1<<18)
#define EXIT_GPIO35           (1<<19)
#define EXIT_GPIO36           (1<<20)
#define EXIT_GPIO37           (1<<21)
#define EXIT_GPIO38           (1<<22)
#define EXIT_GPIO39           (1<<23)

#define EXIT_GPIO48           (1<<24)
#define EXIT_GPIO49           (1<<25)
#define EXIT_GPIO50           (1<<26)
#define EXIT_GPIO51           (1<<27)
#define EXIT_GPIO52           (1<<28)
#define EXIT_GPIO53           (1<<29)
#define EXIT_GPIO54           (1<<30)
#define EXIT_GPIO55           (1<<31)

void gpio_set_irq_mode(INT32U pin, INT32U mode);
void gpio_set_irq_pol(INT32U pin, INT32U pol);
void gpio_irq_en(INT32U pin);
void gpio_irq_clear(INT32U pin);
void gpio_irq_clear_all(void);
void gpio_exter_init(void);
void gpio_exter_en(void);
#endif
