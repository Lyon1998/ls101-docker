#include"Main.h"

/*	
 *  使用定时器,精准制作延时函数
 */

INT32U get_count(void)
{
	// 获取计数器中的值
	INT32U tem_count = 0;
	asm volatile("mfc0	%0, $9":"=r" (tem_count));
	return tem_count;
}

//计数开始
void start_count(TIMER_COUNT *timer_count)
{
	timer_count->begin = 0;
	timer_count->end = 0;
	//将计数初始值放到start中
	timer_count->begin = get_count();
}

//计数结束,将两次计数的差值保存在count中
INT32U stop_count(TIMER_COUNT *timer_count)
{
	//将计数结束的值放到end中
	timer_count->end = get_count();
	if(timer_count->end >= timer_count->begin)
	{
		return (timer_count->end - timer_count->begin);
	}
	else
	{
		INT32U end = -1;
		return (end - timer_count->begin + timer_count->end);
	}
}

void delay_cycle(INT32U num)
{
	INT32U count = 0;
	TIMER_COUNT timer_count = {0, 0};

	Set_Timer_int();
	start_count(&timer_count);

	while (count < num)
	{
		count = stop_count(&timer_count);
	}
}

void delay_us(INT32U x)	    //us单位 
{
	delay_cycle(x*8);
}

void delay_ms(INT32U x)  	//ms单位 
{
	delay_cycle(x*8000);
}

void delay_s(INT32U x)  	//s单位 
{
	delay_ms(x*1000);
}
















