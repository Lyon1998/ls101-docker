#include"Main.h"

//外部8M时钟配置
void os_clock_init(void)
{
	// select ext8m
	PMU_ChipCtrl &= ~(1<<7);
	PMU_ChipCtrl |= (1<<6);
	
	delay_ms(10);     ////////////////////////不加延时，睡眠起来后会时钟失效
	while((PMU_CmdSts&(1<<31)) == 1)
	{
		PMU_ChipCtrl &= ~(1<<7);
	}
	PMU_ChipCtrl|=(1<<7);
}

//32k外部时钟配置，上电只配置一次
void os_clock_ext32k(void)
{
	INT32U counttime=0;
	PMU_ChipCtrl &= ~(1<<5);
	
	do
	{
		wdt_dog_feed();
		PMU_ChipCtrl &= ~(1<<5);
		delay_us(30);
		PMU_ChipCtrl |= (1<<5); 
		delay_us(30);
		if(counttime++ > 3000)
			break;
	}while((PMU_CmdSts&(1<<29)) == 1);
}
