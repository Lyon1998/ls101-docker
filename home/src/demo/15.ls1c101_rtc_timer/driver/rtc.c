#include"Main.h"

void rtc_read(struct rtc_time *Rtime)
{
	INT32U Rtc_time1 = 0;
	INT32U Rtc_time2 = 0;
	
	Rtc_time1 = RTC_RTC1;
	Rtc_time2 = RTC_RTC0;
	
	Rtime->year = (Rtc_time1&0xFE00)>>9;
	Rtime->mon =  (Rtc_time1&0x1E0)>>5;
	Rtime->day =  (Rtc_time1&0x1F);
	printf("date: 20%2d-%2d-%2d",Rtime->year,Rtime->mon,Rtime->day);
	Rtime->hour = (Rtc_time2&0x1F0000)>>16;
	Rtime->min =  (Rtc_time2&0xFC00)>>10;
	Rtime->sec =  (Rtc_time2&0x3F0)>>4;
	printf("\t time: %2d:%2d:%2d\n\r",Rtime->hour,Rtime->min,Rtime->sec);
}

void rtc_write(struct rtc_time *Wtime)
{
	if(((Wtime->mon) > 12)||((Wtime->mon) <= 0))
		Wtime->mon = 1;
	if(((Wtime->day) > 31)||((Wtime->day) <= 0))
		Wtime->day = 1;
		
    RTC_FREQ = (0x7ff << 16) | 0x1;                                          // 32768Hz -> 16Hz
    RTC_RTC0 = (((Wtime->hour)%24)<<16) | (((Wtime->min)%60)<<10) | (((Wtime->sec)%60)<<4) | (((Wtime->week)%100)<<0);// 设置初始时间时、分、秒
    RTC_RTC1 = ((Wtime->year)<<9) | (((Wtime->mon)%13)<<5) | (((Wtime->day)%32)<<0); 				 // 设置初始时间年、月、日
    RTC_CFG = 0x80000000; 		
}



