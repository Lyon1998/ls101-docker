#include"Main.h"

//定时器中断
void timer_int(void)
{
	TIMER_CFG = 0;       //清零配置寄存器
	TIMER_CNT = 0;       //清零计数值
	TIMER_CMP = 8000000; //比较值设置为1s
	TIMER_STP = 16000000; //步进值也设置为2s
	TIMER_CFG = 0x07;    //开始计数,中断使能,周期触发
	INT_EN |= 0x01;      //使能中断
}

void rtc_timer_count(INT32U settime,INT32U systime)
{
	NO_INIT INT32U time = 0;
	
	time = (PMU_Count & 0xfffff);

	if(time < systime)
	{
		time += 0x100000;
	}

	if((time - systime) > (256*settime))  			//RTC时间计数器每1/256s加一次
	{
		printf("-------timer handle-------\n");	
		get_current_timer = (PMU_Count & 0xfffff);  //重新开锁定时
	}
	else
	{
		printf("-------sys running-------\n");		
		delay_s(1);
	}
}
			
