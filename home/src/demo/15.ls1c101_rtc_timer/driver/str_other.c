#include"Main.h"

int memcmp(const void * cs,const void * ct,int count)
{
	const unsigned char *su1, *su2;

	for( su1 = cs, su2 = ct;count > 0; ++su1, ++su2, --count)
	{
		if (*su1 != *su2)
		{
			return 1;
		}
	}
	return 0;
}

void *memcpy(void *s1, const void *s2, int n)
{
	const unsigned char *psrc = s2;
	volatile unsigned char *pdst = s1;

	while (n-- > 0)
		*pdst++ = *psrc++;
	return s1;
}

void *memset(void * s,int c, int count)
{
	unsigned char *xs = (unsigned char *) s;

	while (count--)
		*xs++ = c;

	return s;
}

unsigned char my_strstr(const unsigned char *str, const unsigned char *sub_str,unsigned char num)
{
	unsigned char cont = 0;
	const unsigned char *p,*q;
	while(*str != '\0')
	{
		cont = 0;
		p    = str;
		q    = sub_str;
		if(*p == *q)
		{
			while(*p && *q && *p == *q)
			{
				printf("str = %d sub = %d\n\r",*p,*q);
				p++;
				q++;
				cont += 1;
				if(cont == num)
					return 1;
			}
			printf("\n\r");
		}
		str++;
	}
	return 0;
}

int atoi(char *pstr)
{
	int Ret_Integer = 0;  
	int Integer_sign = 1;
	if(pstr == NULL)  
	{  
		return 0;  
	}  

	if(*pstr == '-')  
	{  
		Integer_sign = -1;  
	}  
	if(*pstr == '-' || *pstr == '+')  
	{
		pstr++;  
	}

	while(*pstr >= '0' && *pstr <= '9')  
	{  
		Ret_Integer = Ret_Integer * 10 + *pstr - '0';  
		pstr++;
	}  
	Ret_Integer = Integer_sign * Ret_Integer;
	
	return Ret_Integer;
}

void itoa(char chWord[], int Num)
{
        int i=0,j;
        char chTemp;
        if(Num == 0)
        {
            chWord[i]='0'+Num%10;
            i++;
        }
        while(Num!=0)
        {
                chWord[i]='0'+Num%10;
                i++;
                Num=Num/10;

        }
        chWord[i]='\0';
        for(j=0;j<i/2;j++)
        {
                chTemp=chWord[j];
                chWord[j]=chWord[i-1-j];
                chWord[i-1-j]=chTemp;
        }
        return ;
}

char *strcat(char *dst, const char *src)
{
	char *d;

	if (!dst || !src)
		return (dst);

	d = dst;
	for (; *d; d++);
	for (; *src; src++)
		*d++ = *src;
	*d = 0;
	return (dst);
}
int strcmp(const char *s1, const char *s2)
{
	while (*s1 == *s2++)
		if (*s1++ == 0)
			return (0);
	return (*(const unsigned char *)s1 - *(const unsigned char *)--s2);
}

char * strcpy(char *dest, const char *src)
{
	char* r=dest;
	while((*dest++ = *src++)!='\0');
	return r;
}


int strlen(const char * p)
{
    int len=0;
    while(*p++)
        len++;
    return len;
}


