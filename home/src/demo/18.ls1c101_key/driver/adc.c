#include"Main.h"

//执行一次电压检测，参考电压为芯片VIO引脚的输入电压，
//返回测量通道channel（输入电压）相对于参考电压的比值。
INT32U ls101_adc_mensure(volatile char channel)
{
	//WriteKeyReg(spPMU_ChipCtrl->adci0_ien,0);    //关闭ADCI0为模拟信号
	spPMU_ChipCtrl->adci0_ien = 0;
	
	PMU_AdcCtrl = 0x100 | channel;                //开始测量与选择测量端口,2分频
	while((PMU_AdcCtrl & (1<<8)));               //判断测量是否结束
	
	INT32U result = PMU_AdcDat;                  //测量结果整合
	
	return result;
}

void insert_sort(INT32U num[], int n){
    int i,j;
	INT32U key;
	for(j=1;j<n;j++)
	{
		key=num[j];
		for(i=j;i>0 && num[i -1] > key;i--)
		{
			num[i]=num[i-1];
		}
		num[i]=key;
	} 
}

void detect_power(void)
{
	INT8U  k = 0;
	INT32U result1  = 0x00;
	INT32U result0  = 0x00;
	INT32U res1[10] = {0x00};
	INT32U res0[10] = {0x00};

	ls101_adc_mensure(0);              //ADCIN
	for(k=0;k<10;k++)
	{
		res0[k] = ls101_adc_mensure(0);//ADCIN
		res1[k] = ls101_adc_mensure(3);//1.0V
	}

	insert_sort(res1,10);
	insert_sort(res0,10);

	for(k=3;k<7;k++)
	{
		result1 += res1[k];
		result0 += res0[k];
	}

	printf("result1:");
	for(k=0;k<10;k++)
	{
		printf("%d  ",res1[k]);
	}
	printf("\n\r");
	printf("result0:");
	for(k=0;k<10;k++)
	{
		printf("%d  ",res0[k]);
	}
	printf("\n\r");
	printf("result1 = %d  result0 = %d \n\r",result1,result0);

	INT32U VIN = 100*result0/result1;
	printf("----------ADC_VIN = %d ----------\n",(INT32U)(VIN));

}

