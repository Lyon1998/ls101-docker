#include"Main.h"

void SOFT_INT(void)
{
}

void TIMER_8M_INT(void)
{
}

/*
 * 主要处理系统进入睡眠以后，如果没有外部中断和触摸按键，睡眠下定时喂狗，防止系统复位 
*/
void TIMER_WAKE_INT(void)
{
}

void TOUCH(void)
{
}

void UART2_INT(void)
{
}

void BAT_FAIL(void)
{
}

void INTC(void)
{
}

/******* IO中断 *******/
void RING(void)
{

}

/******	查询中断源 ********/
void IRQ_Exception()
{
    INT32U bak = 0;
    INT32U irq_no = 0;
    asm volatile("mfc0  %0, $13":"=r" (bak));

    for(irq_no = 8; irq_no < 16; irq_no++)
    {
    	if((bak>>irq_no) & 0x1)
    	{
    		SYS_IrqHandle[irq_no-8]();//进入中断处理程序
    	}
    }
}

