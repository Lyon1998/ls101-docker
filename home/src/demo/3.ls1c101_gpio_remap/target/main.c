#define GLOBAL

#include"Main.h"

int main(void)
{
	PMU_WdtCfg = 0x80007fff;                   //看门够设置最大时间
	
	gpio_pin_remap(GPIO_PIN_1,GPIO_LOW);      //设置该管脚为GPIO
	gpio_pin_remap(GPIO_PIN_2,GPIO_LOW);      //设置该管脚为GPIO
	gpio_pin_remap(GPIO_PIN_3,GPIO_LOW);      //设置该管脚为GPIO
	while(1)
	{
		gpio_write_pin(GPIO_PIN_1,GPIO_LOW);   //蓝
		gpio_write_pin(GPIO_PIN_2,GPIO_HIGH);  //LED灭
		gpio_write_pin(GPIO_PIN_3,GPIO_HIGH);  //LED亮
		delay_s(2);
		gpio_write_pin(GPIO_PIN_1,GPIO_HIGH);  //LED灭
		gpio_write_pin(GPIO_PIN_2,GPIO_LOW);   //红
		gpio_write_pin(GPIO_PIN_3,GPIO_HIGH);  //LED灭
		delay_s(2);
		gpio_write_pin(GPIO_PIN_1,GPIO_HIGH);  //LED灭
		gpio_write_pin(GPIO_PIN_2,GPIO_HIGH);  //LED灭
		gpio_write_pin(GPIO_PIN_3,GPIO_LOW);   //绿
		delay_s(2);
	}

	return 0;
}


