#include"Main.h"

/*
 *   串口波特率配置
 *   base_addr： 需要配置的串口基址
 *   frep     :  主频
 *   baud_rate： 波特率
 */
void uart_cfg_div(volatile unsigned char* base_addr, INT32U freq, INT32U baud_rate)
{
    INT8U lcr = base_addr[3];
    INT32U div=0;
    
    div = ((freq << 8) >> 4) / baud_rate;      //[23:16] DL_H  [15: 8] DL_L  [ 7: 0] DL_D
    
    base_addr[3] = 0x80;
    base_addr[2] = div & 0xff;
    div >>= 8;
    base_addr[0] = div & 0xff;
    div >>= 8;
    base_addr[1] = div & 0xff;
    base_addr[3] = lcr; // restore lcr
    
}

void uart0_init(void)
{
	gpio_pin_remap(GPIO_PIN_6,GPIO_FUNC_MAIN);          //管脚复用为RX
	gpio_pin_remap(GPIO_PIN_7,GPIO_FUNC_MAIN);          //管脚复用为TX
	
	Uart0_FCR = FIFO_TRIGGER_1 | FIFO_TX_RST | FIFO_RX_RST; 
    Uart0_LCR = CLCR_DLAB;      //Baud rate set as 115200
	uart_cfg_div(UART0_BASEADDR,OS_CLOCK_FREQ,115200);

    Uart0_LCR = CLCR_8BITS; 
    Uart0_IER = 0x0;

    INT_POL |= UART0_INT_POL;   //uart interrpt setting
    INT_EN  |= UART0_INT_EN;

    unsigned char data;
	data = Uart0_RxData;
    Uart0_IER |= 0x01;
}


void uart1_init(void)
{
	gpio_pin_remap(GPIO_PIN_8,GPIO_FUNC_MAIN);          //管脚复用为RX
	gpio_pin_remap(GPIO_PIN_9,GPIO_FUNC_MAIN);          //管脚复用为TX
	
	Uart1_FCR = FIFO_TRIGGER_1 | FIFO_TX_RST | FIFO_RX_RST; 
    Uart1_LCR = CLCR_DLAB;      //Baud rate set as 115200
#if 0
    Uart1_DL_H = 0x0;
    Uart1_DL_L = 0x4;
	Uart1_DL_D = 0x57;            //8M
#else
	uart_cfg_div(UART1_BASEADDR,OS_CLOCK_FREQ,115200);
#endif
    Uart1_LCR = CLCR_8BITS; 
    Uart1_IER = 0x0;

    INT_POL |= UART1_INT_POL;   //uart interrpt setting
    INT_EN  |= UART1_INT_EN;

    unsigned char data;
	data = Uart1_RxData;
    Uart1_IER |= 0x01;
}


void uart2_init(void)
{
	gpio_pin_remap(GPIO_PIN_38,GPIO_FUNC_FIRST);          //管脚复用为RX
	gpio_pin_remap(GPIO_PIN_39,GPIO_FUNC_FIRST);          //管脚复用为TX
	
#if 0 //UART_BAU_4800
    Uart2_SAMPLE_CTRL = 0x36; //4800
#else
    Uart2_SAMPLE_CTRL = 0x23;
#endif
    Uart2_LCR = CLCR_DLAB;
    Uart2_DL_H = 0x0;
    Uart2_DL_L = 0x01;
    Uart2_DL_D = 0x23;
    Uart2_LCR = CLCR_8BITS; 
    Uart2_FCR = FIFO_TRIGGER_1 | FIFO_TX_RST | FIFO_RX_RST;
    Uart2_IER = 0x00;

	PMU_CmdSts |= 1<<10;    //enable interrupt
	while(((Uart2_STATUS >> 6) & 0x01)); //CLK32K_RST is 0
	INT8U data;
	data = Uart2_RxData;    //clear the receive fifo
	Uart2_IER |= 0x01;      //enable uart2 rx int and tx int
}

void uart1_send(INT8U str)
{
	while (!(Uart1_LSR & 0x20));
    Uart1_TxData = str;
}

void uart0_send(INT8U str)
{
	while (!(Uart0_LSR & 0x20));
    Uart0_TxData = str;
}

void uart2_send(INT8U str)
{
	while (!(Uart2_LSR & 0x20));
    Uart2_TxData = str;
}



