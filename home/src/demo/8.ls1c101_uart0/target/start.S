

/*************************************************

    1 这个start.s已经禁止了总中断
	2 禁止了TIMER中断使能，但是代码从头开始执行时，TMIER中断的标志位还是会存在的
	  因此在中断处理中 忽略TMER中断标志位就行。

*************************************************/

/************************************/
/*			       C入口*/
.extern main
.extern IRQ_Exception
/************************************/

/************************************/
/*	     bss段起始地址和结束地址*/
.extern __bss_start
.extern __bss_end
/************************************/

#include "start.h"

	.set noreorder

	.globl _RAM_BSS
	_RAM_BSS = MY_BSS
	.bss
        .globl tmpgp
tmpgp:
	 .word 0

	.globl _start
	.text

_start:
    la		gp, tmpgp
	li		sp, 0xa0000ffc
	li		t0, 0x8000000         #disable cp0_count, no 8M timer_interrupt
	mtc0	t0, CP0_CAUSE
#	li      t0, 0x30400000        #中断入口地址在0xbfc00380
	li      t0, 0x10400000        #中断入口地址在0xbfc00380
    mtc0    t0, CP0_STATUS
    .set mips32r2
    ehb
    .set mips0
#	li		t0, 0x00000000         #set fp reg, it's necessory
#	ctc1	t0, $31
#	nop

	/* disable NMI */
#	li		t0, 0xbfea8010
#	li		t1, 0x5a
#	sb		t1, 0(t0)
#	li		t1, 0xa5
#	sb		t1, 0(t0)
#	li		t1, 0x0
#	sb		t1, 0(t0)

InitStack:
	/*设置sp*/
	li		t0, 0xa0000ffc
	move	sp, t0
	nop

	/* clean 8M时钟中断 */
	li		t0, 1
	mtc0	t0, CP0_COMPARE

C_MAIN:
	bal		main		#跳到main入口
	nop

loop:					#如果main函数返回，则死循环
	b	loop
	nop
/********************************************************
***标号Exception以上的代码反汇编不能超过0Xbfc00380******
//	中断异常入口地址，确保是0xbfc00380。
//	芯片跳到0xbfc00380后，硬件上自动就禁止了总中断
//	然后中断返回后的eret指令，硬件上自动就把总中断打开了
********************************************************/

/**************************异常处理**********************************/
  .set  mips3
  .org  0x380
  .ent  Exception
Exception:
.set noat
	SAVE_ALL

	bal		IRQ_Exception	#跳到中断处理中
	nop

leave:
	LOAD_ALL
	eret
	nop
.set at
  .end  Exception
/******************************************************************/
