#ifndef _DELAY_H_
#define _DELAY_H_

typedef struct 
{
	INT32U begin;
	INT32U end;
}TIMER_COUNT;

INT32U		get_count(void);
void 		start_count(TIMER_COUNT *timer_count);
INT32U 		stop_count(TIMER_COUNT *timer_count);
void 		delay_cycle(INT32U num);
void 		delay_us(INT32U x);		    //us单位,精准延时
void 		delay_ms(INT32U x);		    //ms单位,精准延时
void 		delay_s(INT32U x);  	    //s 单位,精准延时


#endif
