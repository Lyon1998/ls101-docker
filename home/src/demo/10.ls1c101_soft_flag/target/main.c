#define GLOBAL

#include"Main.h"

int main(void)
{
	PMU_WdtCfg = 0x80007fff;          //看门狗设置最大时间
	os_clock_init();                  //使用外部8M时钟
	EnableInt();                      //使能全局中断
	uart1_init();                     //串口初始化
	delay_ms(10);
	if(spPMU_ChipCtrl->soft_flag == 0)                     //通过软件标志判断是否为第一次上电
    {
		printf("-------power supply wakeup------\n");      //上电唤醒时,执行这里函数,常用放置一些系统只需要初始一次的函数或变量
		spPMU_ChipCtrl->soft_flag = 0xa;                   //设置软件标志
    }
    else
    {
		printf("-------sleep wake up------\n");            //休眠唤醒则执行这里的函数
	}
	
	while(1)
	{
		
	}

	return 0;
}


