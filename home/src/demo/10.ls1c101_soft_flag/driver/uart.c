#include"Main.h"

/*
 *   串口波特率配置
 *   base_addr： 需要配置的串口基址
 *   frep     :  主频
 *   baud_rate： 波特率
 */
void uart_cfg_div(volatile INT8U* base_addr, INT32U freq, INT32U baud_rate)
{
    INT8U lcr = base_addr[3];
    INT32U div=0;
    
    div = ((freq << 8) >> 4) / baud_rate;      //[23:16] DL_H  [15: 8] DL_L  [ 7: 0] DL_D
    
    base_addr[3] = 0x80;
    base_addr[2] = div & 0xff;
    div >>= 8;
    base_addr[0] = div & 0xff;
    div >>= 8;
    base_addr[1] = div & 0xff;
    base_addr[3] = lcr; // restore lcr
}

void uart1_init(void)
{
	gpio_pin_remap(GPIO_PIN_8,GPIO_FUNC_MAIN);          //管脚复用为RX
	gpio_pin_remap(GPIO_PIN_9,GPIO_FUNC_MAIN);          //管脚复用为TX
	
	Uart1_FCR = FIFO_TRIGGER_1 | FIFO_TX_RST | FIFO_RX_RST; 
    Uart1_LCR = CLCR_DLAB;      //Baud rate set as 115200
	uart_cfg_div(UART1_BASEADDR,OS_CLOCK_FREQ,115200);
    Uart1_LCR = CLCR_8BITS; 
    Uart1_IER = 0x0;

   // INT_POL |= UART1_INT_POL;   //uart interrpt setting
   // INT_EN  |= UART1_INT_EN;

   // unsigned char data;
	//data = Uart1_RxData;
   // Uart1_IER |= 0x01;
}

void uart1_send(INT8U str)
{
	while (!(Uart1_LSR & 0x20));
    Uart1_TxData = str;
}


