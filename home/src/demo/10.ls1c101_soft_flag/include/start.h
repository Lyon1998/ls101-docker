#ifndef START_H_
#define START_H_

#define MY_BSS  0xa0001000

/*
 * Symbolic register names for 32 bit ABI, General Register
 */
#define zero    $0  /* wired zero */
#define AT  $1  /* assembler temp  - uppercase because of ".set at" */
#define v0  $2  /* return value */
#define v1  $3
#define a0  $4  /* argument registers */
#define a1  $5
#define a2  $6
#define a3  $7
#define t0  $8  /* caller saved */
#define t1  $9
#define t2  $10
#define t3  $11
#define t4  $12
#define t5  $13
#define t6  $14
#define t7  $15
#define s0  $16 /* callee saved */
#define s1  $17
#define s2  $18
#define s3  $19
#define s4  $20
#define s5  $21
#define s6  $22
#define s7  $23
#define t8  $24 /* caller saved */
#define t9  $25
#define jp  $25 /* PIC jump register */
#define k0  $26 /* kernel scratch */
#define k1  $27
#define gp  $28 /* global pointer */
#define sp  $29 /* stack pointer */
#define fp  $30 /* frame pointer */
#define s8  $30 /* same like fp! */
#define ra  $31 /* return address */

/*
 * Coprocessor 0 register names
 */
#define CP0_INDEX       $0
#define CP0_RANDOM      $1
#define CP0_ENTRYLO0    $2
#define CP0_ENTRYLO1    $3
#define CP0_CONF        $3
#define CP0_CONTEXT     $4
#define CP0_PAGEMASK    $5
#define CP0_WIRED       $6
#define CP0_INFO        $7
#define CP0_BADVADDR    $8
#define CP0_COUNT       $9
#define CP0_ENTRYHI     $10
#define CP0_COMPARE     $11
#define CP0_STATUS      $12
#define CP0_CAUSE       $13
#define CP0_EPC         $14
#define CP0_PRID        $15
#define CP0_CONFIG      $16
#define CP0_LLADDR      $17
#define CP0_WATCHLO     $18
#define CP0_WATCHHI     $19
#define CP0_XCONTEXT    $20
#define CP0_FRAMEMASK   $21
#define CP0_DIAGNOSTIC  $22
#define CP0_DEBUG       $23
#define CP0_DEPC        $24
#define CP0_PERFORMANCE $25
#define CP0_ECC         $26
#define CP0_CACHEERR    $27
#define CP0_TAGLO       $28
#define CP0_TAGHI       $29
#define CP0_ERROREPC    $30
#define CP0_DESAVE      $31

/****************************************************************************/
#define SAVE_ALL\
    addi    sp, -132;\
    sw  $0, 0x80(sp);\
    sw  $1, 0x7c(sp);\
    sw  $2, 0x78(sp);\
    sw  $3, 0x74(sp);\
    sw  $4, 0x70(sp);\
    sw  $5, 0x6c(sp);\
    sw  $6, 0x68(sp);\
    sw  $7, 0x64(sp);\
    sw  $8, 0x60(sp);\
    sw  $9, 0x5c(sp);\
    sw  $10, 0x58(sp);\
    sw  $11, 0x54(sp);\
    sw  $12, 0x50(sp);\
    sw  $13, 0x4c(sp);\
    sw  $14, 0x48(sp);\
    sw  $15, 0x44(sp);\
    sw  $16, 0x40(sp);\
    sw  $17, 0x3c(sp);\
    sw  $18, 0x38(sp);\
    sw  $19, 0x34(sp);\
    sw  $20, 0x30(sp);\
    sw  $21, 0x2c(sp);\
    sw  $22, 0x28(sp);\
    sw  $23, 0x24(sp);\
    sw  $24, 0x20(sp);\
    sw  $25, 0x1c(sp);\
    sw  $26, 0x18(sp);\
    sw  $27, 0x14(sp);\
    sw  $28, 0x10(sp);\
    sw  $29, 0xc(sp);\
    sw  $30, 0x8(sp);\
    sw  $31, 0x4(sp)


#define LOAD_ALL\
    lw  $0, 0x80(sp);\
    lw  $1, 0x7c(sp);\
    lw  $2, 0x78(sp);\
    lw  $3, 0x74(sp);\
    lw  $4, 0x70(sp);\
    lw  $5, 0x6c(sp);\
    lw  $6, 0x68(sp);\
    lw  $7, 0x64(sp);\
    lw  $8, 0x60(sp);\
    lw  $9, 0x5c(sp);\
    lw  $10, 0x58(sp);\
    lw  $11, 0x54(sp);\
    lw  $12, 0x50(sp);\
    lw  $13, 0x4c(sp);\
    lw  $14, 0x48(sp);\
    lw  $15, 0x44(sp);\
    lw  $16, 0x40(sp);\
    lw  $17, 0x3c(sp);\
    lw  $18, 0x38(sp);\
    lw  $19, 0x34(sp);\
    lw  $20, 0x30(sp);\
    lw  $21, 0x2c(sp);\
    lw  $22, 0x28(sp);\
    lw  $23, 0x24(sp);\
    lw  $24, 0x20(sp);\
    lw  $25, 0x1c(sp);\
    lw  $26, 0x18(sp);\
    lw  $27, 0x14(sp);\
    lw  $28, 0x10(sp);\
    lw  $29, 0xc(sp);\
    lw  $30, 0x8(sp);\
    lw  $31, 0x4(sp);\
    addi    sp, 132

/****************************************************************************/
#endif

