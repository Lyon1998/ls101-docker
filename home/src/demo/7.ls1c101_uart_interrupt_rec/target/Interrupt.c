#include"Main.h"

void SOFT_INT(void)
{
}

void TIMER_8M_INT(void)
{
}

void TIMER_WAKE_INT(void)
{
}

void TOUCH(void)
{
}

void UART2_INT(void)
{
}

void BAT_FAIL(void)
{
}

void INTC(void)
{
	INT8U IntReg = INT_OUT;
	
	if(IntReg&UART1_INT_OUT)    					//Uart1
	{
		INT8U uart_sr = Uart1_IIR;	;

		if(uart_sr&0x04)
        {
			while(Uart1_LSR&0x1)
			{
				uart_rec[uart_rec_len] = Uart1_RxData;
				uart_rec_len = (uart_rec_len+ 1) % REV_NUM;
				if(uart_rec_len>9)
				{
					printBuf(uart_rec,10);
					uart_rec_len = 0;
				}
			}
        }
       	INT_CLR = UART1_INT_CLR;
	}
	
	if(IntReg&UART0_INT_OUT)    //Uart0
	{
		INT_CLR = UART0_INT_CLR;
	}
	
	INT_CLR = 0xff;
	
}

/******* IO中断 *******/
void RING(void)
{

}

/******	查询中断源 ********/
void IRQ_Exception()
{
    INT32U bak = 0;
    INT32U irq_no = 0;
    asm volatile("mfc0  %0, $13":"=r" (bak));

    for(irq_no = 8; irq_no < 16; irq_no++)
    {
    	if((bak>>irq_no) & 0x1)
    	{
    		SYS_IrqHandle[irq_no-8]();//进入中断处理程序
    	}
    }
}

