#ifndef _PRINTF_H_
#define _PRINTF_H_

#define max(a,b) (((a)>(b))?(a):(b))

#if 1							
#define printf(fmt,args...)	           myprintf(fmt ,##args)
#else
#define printf(fmt,args...)    
#endif

void 		myputchar(unsigned char chr);
int 		printbase(long v,int w,int base,int sign);
int 		puts(char *s);
int 		myprintf(char *fmt,...);
void 		printBuf(INT8U *buf,INT32U len);

#endif
