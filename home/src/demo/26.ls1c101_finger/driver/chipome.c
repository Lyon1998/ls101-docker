#include"Main.h"

void print_fpBuf(INT8U *buf)
{
    int i;
    for (i = 0; i < 24; i++) 
    {
         if (i % 32 == 0) 
         {
             printf("[%4x]:", i);
         }
         printf("%2x ", buf[i]);
    }
    printf("\r\n");
}

//自动搜索
const INT8U Identify[24] = {
	0x55,0xAA,	//包标识
	0x02,0x01,  //命令 1:N搜索
	0x00,0x00,  //包长度
	0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00, //数据包
	0x02,0x01   //校验码
};
	
//检测指纹是否有按下
const INT8U  Have_Finger[24] = 	{
	0x55,0xAA,	//包标识
	0x13,0x01,  //命令是否有手指
	0x00,0x00,  //包长度
	0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00, //数据包
	0x13,0x01   //校验码
};

//检测连接是否正常
const INT8U  Connection[24] = {
	0x55,0xAA,	//包标识
	0x50,0x01,  //命令连接是否正常
	0x00,0x00,  //包长度
	0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00, //数据包
	0x50,0x01   //校验码
};

//获取可注册的ＩＤ号
const INT8U  Get_ID[24] = {
	0x55,0xAA,	//包标识
	0x07,0x01,  //获取可注册ＩＤ号
	0x00,0x00,  //包长度
	0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00, //数据包
	0x07,0x01   //校验码
};
	
//取消命令
const INT8U Cancel[24] = {
	0x55,0xAA,	//包标识
	0x30,0x01,  //命令取消命令
	0x00,0x00,  //包长度
	0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00, //数据包
	0x30,0x01   //校验码
};

//清空指纹
const INT8U  Clear_All_Template[24] = {
	0x55,0xaa,	//包标识
	0x06,0x01,  //命令 清空指纹
	0x00,0x00,  //包长度
	0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00, //数据包
	0x06,0x01   //校验码
};	
/*
//睡眠命令
const INT8U  Standby[24] = {
	0x55,0xAA,	//包标识
	0x17,0x01,  //命令进入睡眠
	0x00,0x00,  //包长度
	0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00, //数据包
	0x17,0x01   //校验码
};

//修改超时时间
const INT8U Time_Out[24] = {
	0x55,0xaa,	//包标识
	0x0E,0x01,  //命令 修改超时时间
	0x02,0x00,  //包长度
	0x1F,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00, //数据包
	0x2F,0x01   //校验码
};
*/

void finger_init(void)
{
	gpio_pin_remap(GPIO_PIN_35,GPIO_FUNC_MAIN);
	uart1_init();							    //初始化串口 与指纹头通信使用	
	delay_ms(100);
}

/*
 * 数据格式 ：
 * 校验和=包识别码+命令字+包长度+包内容
*/

INT32U Sum(INT8U *buf)
{
	INT32U i;
    INT32U sum = 0;
	for (i = 0; i < 22; i++) 
	{
		sum += buf[i];  
	}
	return sum;
}


INT8U Cmd_Sum_Chk(INT8U *buf)
{
	INT32U sum = 0;

	print_fpBuf(buf);
	sum = Sum(buf);
	
	if((buf[0] == 0xAA)&&(buf[1] == 0x55)&&(((sum>>8)&0xff) == buf[23])&&((sum&0xff) == buf[22]))	
		return CMD_ACK_OK;
	
	return CMD_ACK_ERR;	
}

//刻录新的指纹
void Fp_Enroll(INT32U user_id)
{
	INT32U  sum = 0;
	INT8U  Enroll[24] = 
	{
		0x55,0xAA,	//包标识
		0x03,0x01,  //命令3次刻录
		0x02,0x00,  //包长度
		0x00,0x00,  //用户ID号
		0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,		//数据包
		0x00,0x00   //校验码
	}; //注册为ID的用户

	Enroll[6] = user_id&0xff;
	Enroll[7] = (user_id>>8)&0xff;
	
	sum = Sum(Enroll);
	
	Enroll[22] = sum&0xff;
	Enroll[23] = (sum>>8)&0xff;
	
	uart1_send(Enroll);
}

//删除指纹模板
void DeleteChar(INT32U user_id)
{
	INT32U sum = 0;
	INT8U  Clear_Template[24] = 
	{
		0x55,0xAA,	//包标识
		0x05,0x01,  //命令  删除指定用户 ID
		0x02,0x00,  //包长度
		0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,		//数据包
		0x00,0x00   //校验码
	};
	
	Clear_Template[6] = user_id&0xff;
	Clear_Template[7] = (user_id>>8)&0xff;
	
	sum = Sum(Clear_Template);
	
	Clear_Template[22] = sum&0xff;
	Clear_Template[23] = (sum>>8)&0xff;
	
	uart1_send(Clear_Template);
}

void Send_Cmd(void)
{
	wdt_dog_feed();  
	uart_rev_p.put_pos = 0x00;
	memset(&uart_rev_p,0,sizeof(uart_rev_p));
	printf("send cmd fp_status.fp_cmd 0x%x \r\n",fp_status.fp_cmd);
	switch(fp_status.fp_cmd)
	{
		case FINGER_CMD_IDENTIFY:		//单次匹配
		{
			uart1_send(Identify);
		}
		break;
		case FINGER_CMD_GETID:			 //获ＩＤ
		{
			uart1_send(Get_ID);
		}		
		break;
		case FINGER_CMD_DETECT:			//判断是否有手指
		{	
			uart1_send(Have_Finger);
		}
		break;
		case FINGER_CMD_RECORD:			//刻录
		{
			Fp_Enroll(fp_status.fp_read_id);
		}		
		break;
		case FINGER_CMD_DELETE:			//删除
		{
			DeleteChar(fp_status.fp_read_id);
		}		
		break;
		case FINGER_CMD_CONNECTION:		//通路
		{
			uart1_send(Connection);
		}		
		break;
		case FINGER_CMD_CLEAR:			//清空
		{
			uart1_send(Clear_All_Template);
		}		
		break;
		case FINGER_CMD_CANCEL:			//取消
		{
			uart1_send(Cancel);
		}		
		break;
		case FINGER_CMD_IDLE:			//无
		{
		}
		break;
		default:
		break;
	}
}

void Cmd_Back(void)
{
	INT8U  data[24] = {0};
	
	NO_INIT INT32U rlt = 0;
	NO_INIT INT32U cmd_ret = 0;
	NO_INIT INT32U cmd_ver = 0;
	NO_INIT INT32U fp_user_id = 0;

	memcpy(data,&uart_rev_p.backup,24);
	
	printf("cmd back fp_status.fp_cmd 0x%x \r\n",fp_status.fp_cmd);

	rlt = Cmd_Sum_Chk(data);
	cmd_ret = data[6] + data[7];	
	cmd_ver = ((data[3]*256 + data[2]) & 0xffff);
	fp_user_id = (data[9]&0xff)*256 + (data[8]&0xff);
	printf("<<finger_id = %d>>  <<cmd_ret = 0x%x>>  <<cmd_ver=%2x>>  <<rlt = %d>> \n",fp_user_id,cmd_ret,cmd_ver,rlt);

	if((rlt == CMD_ACK_OK) && (cmd_ver == fp_status.fp_cmd))
	{
		switch(fp_status.fp_cmd)
		{
			case FINGER_CMD_IDENTIFY:		 //单次匹配
			{
				if(cmd_ret == ERR_SUCCESS)
				{
					printf("FINGER_CMD_IDENTIFY\r\n");
				}
				else
				{
					switch(fp_user_id)
					{
						case ERR_IDENTIFY:
						{
							
						}	
						break;
						case ERR_ALL_TMPL_EMPTY:
						{
							
						}			
						break;
						default :	
						break;
					}
				}
			}
			break;
			case FINGER_CMD_GETID:			 //获ＩＤ
			{
				if(cmd_ret == ERR_SUCCESS)
				{
					printf("FINGER_CMD_GETID--fp_user_id=%d\r\n",fp_user_id);
				}
			}		
			break;
			case FINGER_CMD_DETECT:			//判断是否有手指
			{
				if(cmd_ret == ERR_SUCCESS)
				{
					if(data[8] == 0x01)    //该位为1则代表有手指按上
					{
						printf("FINGER_CMD_DETECT\r\n");
					}
				}
			}		
			break;
			case FINGER_CMD_RECORD:			//刻录
			{
				if(cmd_ret == ERR_SUCCESS)
				{
					printf("FINGER_CMD_RECORD SUCCESS fp_user_id = %d \n",fp_user_id);
				}
				else
				{
					if(fp_user_id == ERR_DUPLICATION_ID)
					{
						printf("FINGER_CMD_RECORD1 FAILE\n");
					}
					else if(fp_user_id == ERR_BAD_QUALITY)
					{
						printf("FINGER_CMD_RECORD2 FAILE\n");
					}
				}
			}		
			break;
			case FINGER_CMD_DELETE:			//删除
			{
				if(cmd_ret == ERR_SUCCESS)
				{
					printf("FINGER_CMD_DELETE  SUCCESS\r\n");
				}
				else
				{
					if((fp_user_id == ERR_TMPL_EMPTY)||(fp_user_id == ERR_INVALID_TMPL_NO))
					{
						printf("FINGER_CMD_DELETE  FAILE\r\n");
					}
				}
			}	
			break;
			case FINGER_CMD_CONNECTION:		//通路
			{
				if(cmd_ret == ERR_SUCCESS)
				{
					printf("FINGER_CMD_CONNECTION\r\n");
				}
			}		
			break;
			case FINGER_CMD_CLEAR:			//清空
			{
				if(cmd_ret == ERR_SUCCESS)
				{
					printf("FINGER_CMD_CLEAR\r\n");
				}
			}		
			break;
			case FINGER_CMD_CANCEL:			//取消
			{
				if(cmd_ret == ERR_SUCCESS)
				{
					printf("FINGER_CMD_CANCEL\r\n");
				}
			}		
			break;
			case FINGER_CMD_IDLE:			//无
			break;
			default:
			break;
		}
		
	}
}

void finger_test(void)
{
	switch(Fp_key_val)
	{
		case 1:     
		{
			fp_status.fp_cmd = FINGER_CMD_CONNECTION;
			Send_Cmd();
		}
		break;	
		case 2:
		{
			fp_status.fp_cmd = FINGER_CMD_CLEAR;
			Send_Cmd();
		}
		break;	
		case 3:
		{
			fp_status.fp_cmd = FINGER_CMD_GETID;
			Send_Cmd();
		}
		break;	
		case 4:
		{
			fp_status.fp_read_id = 0;
			fp_status.fp_cmd = FINGER_CMD_DELETE;
			Send_Cmd();
		}
		break;	
		case 5:
		{
			fp_status.fp_cmd = FINGER_CMD_CANCEL;
			Send_Cmd();
		}
		break;	
		case 6:
		{
			fp_status.fp_cmd = FINGER_CMD_DETECT;
			Send_Cmd();
		}
		break;
		case 7:
		{
			fp_status.fp_read_id = 0;
			fp_status.fp_cmd = FINGER_CMD_RECORD;
			Send_Cmd();
		}
		break;
		default:
		break;	
	}
	Fp_key_val = 0;
}

