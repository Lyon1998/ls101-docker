#ifndef _UART_H_
#define _UART_H_

#define	FIFO_RX_RST			            0x02	/* reset receive fifo */
#define	FIFO_TX_RST			            0x04	/* reset transmit fifo */
#define	FIFO_TRIGGER_1			        0x08 	/* trigger at 1 chars */       
#define	FIFO_TRIGGER_2			        0x10	/* trigger at 2 chars */       
#define	FIFO_TRIGGER_3			        0x18	/* trigger at 3 chars */        
#define	FIFO_TRIGGER_4			        0x20	/* trigger at 4 char */        
#define	FIFO_TRIGGER_5			        0x28	/* trigger at 5 chars */       
#define	FIFO_TRIGGER_6			        0x30	/* trigger at 6 chars */        
#define	FIFO_TRIGGER_7			        0x38	/* trigger at 7 chars */       
#define	FIFO_TRIGGER_8			        0x40	/* trigger at 8 char */     
#define	FIFO_TRIGGER_9			        0x48	/* trigger at 9 chars */        
#define	FIFO_TRIGGER_10			        0x50	/* trigger at 10 chars */       
#define	FIFO_TRIGGER_11			        0x58	/* trigger at 11 chars */      
#define	FIFO_TRIGGER_12			        0x60	/* trigger at 12 char */       
#define	FIFO_TRIGGER_13			        0x68	/* trigger at 13 chars */      
#define	FIFO_TRIGGER_14			        0x70	/* trigger at 14 chars */      
#define	FIFO_TRIGGER_15			        0x78	/* trigger at 15 chars */      
#define	FIFO_TRIGGER_16			        0x80	/* trigger at 16 chars */       
#define	CLCR_DLAB				        0x80	/* divisor latch */
#define	CLCR_SBREAK				        0x40	/* send break */
#define	CLCR_PZERO				        0x30	/* zero parity */
#define	CLCR_PONE				        0x20	/* one parity */
#define	CLCR_PEVEN				        0x10	/* even parity */
#define	CLCR_PODD				        0x00	/* odd parity */
#define	CLCR_PENAB				        0x08	/* parity enable */
#define	CLCR_NOENAB				        0x00	/* parity no enable */
#define	CLCR_STOPB1				        0x00	/* 1 stop bits */
#define	CLCR_STOPB1P5_5BEC		        0x04	/* 1.5 stop bits */
#define	CLCR_STOPB2_6BEC		        0x05	/* 2 stop bits */
#define	CLCR_STOPB2_7BEC		        0x06	/* 2 stop bits */
#define	CLCR_STOPB2_8BEC		        0x07	/* 2 stop bits */
#define	CLCR_8BITS				        0x03	/* 8 data bits */
#define	CLCR_7BITS				        0x02	/* 7 data bits */
#define	CLCR_6BITS				        0x01	/* 6 data bits */
#define	CLCR_5BITS				        0x00	/* 5 data bits */
#define	TFCNT_LOOPBACK			        0x80	/* loopback */
#define	REV_NUM					        32      //接收最大范围
#define	UART_TX_NUM				        32      //发送最大范围

typedef struct
{	
	INT8U data[REV_NUM];
	volatile INT8U put_pos;
	INT8U backup[24];
}Uart_Rev;

INT8U uart_rec[10];
INT8U uart_rec_len;

void 		uart_cfg_div(volatile INT8U* base_addr, INT32U freq, INT32U baud_rate);
void 		uart0_init(void);
void 		uart1_init(void);
void 		uart0_send(INT8U str);
void 		uart1_send(const INT8U *str);
#endif
