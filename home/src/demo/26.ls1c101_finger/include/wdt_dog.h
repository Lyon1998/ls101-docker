#ifndef _WDT_DOG_H_
#define _WDT_DOG_H_

int odd_test(int num);
void wdt_dog_feed(void);
void wdt_dog_set(INT32U time);

#endif
