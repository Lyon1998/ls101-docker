#ifndef _CHIPOME_H_
#define _CHIPOME_H_

//指纹头CMD模式定义
enum FINGER_MODE
{
    FINGER_CMD_IDLE       = 0x0000,			//空闲模式
    FINGER_CMD_IDENTIFY   = 0x0102, 		//单次匹配模式
    FINGER_CMD_RECORD     = 0x0103,			//录制模式
    FINGER_CMD_DELETE     = 0x0105,			//删除用户
    FINGER_CMD_CLEAR      = 0x0106,			//清空指纹
    FINGER_CMD_GETID      = 0x0107,	 		//获取ＩＤ
    FINGER_CMD_REGISTER   = 0x0108,         //判断是否已注册
    FINGER_CMD_TIMEOUT    = 0x010E,	 		//超时设定
    FINGER_CMD_DETECT     = 0x0113,	 		//判断是否有手指
    FINGER_CMD_CANCEL     = 0x0130,			//取消命令
    FINGER_CMD_CONNECTION = 0x0150,			//通路测试
};

#define CMD_NUM                         24		                   // 命令长度 
#define CMD_ACK_OK				        0
#define CMD_ACK_ERR				        1   

enum FINGER_ERROR
{
	ERR_SUCCESS           = 0x00, 	    //指令处理成功
	ERR_FAIL              = 0x01,       //指令处理失败
	ERR_VERIFY            = 0x11,       //与指定Template 1:1比对失败
	ERR_IDENTIFY          = 0x12,       //已进行1:N搜索但不存在匹配的Template
	ERR_TMPL_EMPTY        = 0x13,       //在指定号码中不存在已登记的Template
	ERR_TMPL_NOT_EMPTY    = 0x14,       //在指定号码中已存在Template
	ERR_ALL_TMPL_EMPTY    = 0x15,       //不存在已登记的Template
	ERR_EMPTY_ID_NOEXIST  = 0x16,       //不存在可登记的Template ID
	ERR_INVALID_TMPL_DATA = 0x18,       //指定的Template Data无效
	ERR_DUPLICATION_ID    = 0x19,       //该指纹已登记
	ERR_BAD_QUALITY       = 0x21,       //指纹图像质量不好
	ERR_TIME_OUT          = 0x23,       //在Timeout时间内没有检测到指纹输入
	ERR_GENERALIZE        = 0x30,       //登记Template的制作失败
	ERR_FP_CANCEL 		  =	0x41,       //指令已被取消
	ERR_INTERNAL 		  = 0x50,       //软件内部错误
	ERR_MEMORY            = 0x51,       //存储错误
	ERR_EXCEPTION         = 0x52,       //软件/硬件异常
	ERR_INVALID_TMPL_NO   = 0x60,       //指定的Template号码无效
	ERR_INVALID_SEC_VAL   = 0x61,       //指定的Security Level值无效
	ERR_INVALID_TIME_OUT  = 0x62,       //指定的Timeout值无效
	ERR_INVALID_BAUDRATE  = 0x63,       //指定的Baud rate值无效
	ERR_INVALID_DUP_VAL   = 0x65,       //指定的DuplicationCheckOption值无效
	ERR_INVALID_PARAM     = 0x70,       //使用了无效参数
	ERR_NO_RELEASE        = 0x71,       //在Identify Free指令执行过程中,识别不成功的指纹未离开
	GD_NEED_FIRST_SWEEP   = 0xFFF1,     //第一次指纹输入等待状态
	GD_NEED_SECOND_SWEEP  = 0xFFF2,     //第二次指纹输入等待状态
	GD_NEED_THIRD_SWEEP   = 0xFFF3,     //第三次指纹输入等待状态
	GD_NEED_FOURTH_SWEEP  = 0xFFF4,     //第四次指纹输入等待状态
	GD_NEED_REALSE_FINGER = 0xFFFA,     //离开手指
	GD_TMPL_NOT_EMPTY 	  = 0x01,       // Template不为空
	GD_TMPL_EMPTY         = 0x00,       // Template为空
};

//负责指纹模块状态数据
typedef struct
{
    INT32U fp_cmd; 				//FP_CMD
    INT32U fp_read_id; 			//1~N
}FpStatusMsg;

FpStatusMsg			fp_status;
Uart_Rev			uart_rev_p;
INT32U              Fp_key_val;

void 		finger_init(void);
INT32U 		Sum(INT8U *buf);
INT8U 		Cmd_Sum_Chk(INT8U *buf);
void 		Fp_Enroll(INT32U user_id);
void 		DeleteChar(INT32U user_id);
void 		Send_Cmd(void);
void 		print_fpBuf(INT8U *buf);
void 		Cmd_Back(void);
void 		finger_test(void);
#endif
