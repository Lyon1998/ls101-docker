#ifndef _KEY_H_
#define _KEY_H_

#define KEY_CESHI       0


INT32U 		key_auto_calibration(void);
void 		key_touch_init(void);
INT32U 		key_read_value(void);
int 		key_count_value_test();
int 		key_count_result_test();
void 		key_one_scan(void);

#endif
