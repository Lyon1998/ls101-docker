#ifndef _FLASH_H_
#define _FLASH_H_

#define FLASH_ERASE_ALL			         0x80000000
#define FLASH_ERASE_CMD			         0xa0000000
#define FLASH_WRITE_CMD			         0xe0000000
#define FLASH_PAGE_LATCH_CLEAR	         0x40000000
#define FLASH_ADDR_MASK			         0x7ffff80

#define FLASH_Command_Daddr_OFFSET		 0
#define FLASH_Command_Daddr_MASK		 0xffff
#define FLASH_Command_Cmd_OFFSET		 28
#define FLASH_Command_Cmd_MASK			 0xf

#define FLASH_Command_erase_page		 (0xa<<FLASH_Command_Cmd_OFFSET)
#define FLASH_Command_prog_page			 (0xe<<FLASH_Command_Cmd_OFFSET)
#define FLASH_Command_clr_page_latch	 (0x4<<FLASH_Command_Cmd_OFFSET)
#define FLASH_Command_check_page		 (0x1<<FLASH_Command_Cmd_OFFSET)
#define FLASH_Command_update_key		 (0xf<<FLASH_Command_Cmd_OFFSET)
#define FLASH_Command_update_bound		 (0x9<<FLASH_Command_Cmd_OFFSET)
#define FLASH_Command_clr_int			 (0x3<<FLASH_Command_Cmd_OFFSET)

#define	LS1D_FLASH_ADDR			       	 0xbf000000
#define	LS1D_FLASH_TOTAL		         0x1ffff		//128KB
#define FLASH_PAGE_SIZE			         128
#define	WORD_WIDTH				         4			//every time for wirte is word width.


void flash_erase_page(INT32U addr);
void user_flash_erase(INT32U addr);
int ls1d_flash_read_page(INT32U addr, INT8U *data, INT32U num);
INT32U ls1d_flash_write(INT32U addr, INT8U *data, INT32U num);
void write_test(void);
void read_test(void);

#endif 

