#include"Main.h"

INT32U key_auto_calibration(void)
{
    int CntRes0[16]={0};
    int CntRes[16] ={0};
    int i,j;
    int min;
    TS_CTRL = 0xfff00000;
    
    for(i=0;i<16;i++)
		TS_CNTADJ(i)=0;
#if 1	
	TS_CNTADJ(4) = (0xff << 16);     //按键不带电阻
	TS_CNTADJ(5) = (0x0 << 16);
	TS_CNTADJ(6) = (0x2f << 16);
	TS_CNTADJ(7) = (0x9f << 16);
	TS_CNTADJ(8) = (0xff << 16);
	TS_CNTADJ(9) = (0xff << 16);
	TS_CNTADJ(10) = (0xef << 16);
	TS_CNTADJ(11) = (0xff << 16);
	TS_CNTADJ(12) = (0xff << 16);
	TS_CNTADJ(13) = (0xff << 16);
	TS_CNTADJ(14) = (0x7f << 16);
	TS_CNTADJ(15) = (0xff << 16);
#endif

    //求16次的累加和
    for (j=0;j<16;j++)
    {
		TS_CTRL = 0xfff00001;
		while (TS_CTRL & 0x01);
	  
        for (i=0;i<16;i++)
        {
           CntRes0[i] += TS_CNTRES(i);
           printf("%d ",TS_CNTRES(i));
        }
        printf("\n\r");
        delay_ms(1);
    }
    
  
    //算出平均值
    for (i=0;i<16;i++)
    {
        CntRes[i]=(CntRes0[i])/16;
    }
    
    //求出最小值
    min = 4096;//CntRes[0];
    for (i=4;i<16;i++)
    {
        if (min > CntRes[i])
        {
			min=CntRes[i];
		}
    }
  
    //计算出修正量并放入修正寄存器
    for (i=4;i<16;i++)
    {
		TS_CNTADJ(i) |= (CntRes[i]-min);				 //调整量放在修正寄存器 
    }

	key_count_value_test();
    TS_CTRL = 0xfff00004;  
	return min;
}

void key_touch_init(void)
{
	printf("---------KEY INIT----------\r\n");
    TS_OSCCFG    = 0x00000411;          
    TS_POLLTIM   = 0x01053c00;                    //激活模式周期，激活模式扫描时间，待机模式周期，去抖测量间隔
    TS_DIFFTHRES = 0x13050387;                    //触摸阈值设置为15,触摸检测使能,按键阈值 
    PMU_CmdSts   |= (1<<9);                       //触摸按键中断使能
    key_auto_calibration();
    TS_CTRL      = 0xfff0b016;                    //关闭单次扫描
    delay_ms(1);
}

INT32U key_read_value(void)
{	
	wdt_dog_feed();             //喂狗
	
    INT32U key_value = (TS_STAT & 0xf0) >> 4;
    printf ("\n --------------key_value = %d----------\r\n", key_value);
    
	switch(key_value)       //调整按键的顺序
	{
		case 13: 	key_value =   9;  	break;
		case 15: 	key_value =   6;  	break;
		case 12: 	key_value =   11;  	break;
		case 14: 	key_value =   3;  	break;
		case  4: 	key_value =   4;  	break;
		case  5: 	key_value =   1;   	break;
		case  6: 	key_value =   7;   	break;
		case  7: 	key_value =   10;  	break;
		case  8: 	key_value =   2;    break;
		case  9: 	key_value =   5; 	break;
		case 10: 	key_value =   8; 	break;
		case 11: 	key_value =   0; 	break;
		default:	key_value =   11;	break;
	}
    
    printf ("\n **********key_value_new = %d************\r\n", key_value);
   
    return key_value;
}


int key_count_value_test()
{
    unsigned int addr;
    int i;
    addr=0xbfeb4050;
  
    for(i=0; i<12; i++)
    {
        if((i%4) == 0)
            printf("\n0x%08x:\t", addr+i*4);
        printf(" %08x ", *(volatile unsigned int*)(addr+i*4));
        }
    return 0;
}

int key_count_result_test()
{
    unsigned int addr;
    int i, num;
    addr=0xbfeb4080;
    num = 8;
    for(i=0; i<num; i++)
    {
        if((i%4) == 0)
            printf("\n0x%08x:\t", addr+i*4);
        printf(" %04d ", *(volatile unsigned int*)(addr+i*4));
    }
    return 0;
}


///////////////////////////////测试时使用/////////////////////////////////
#if KEY_CESHI
//单次扫描测试
void key_one_scan(void)
{
	int CntRes0[16]={0};
    int CntRes[16] ={0};
    int i,j;
    int min;
 
	TS_CTRL = 0xfff00005;
 
	while (TS_CTRL & 0x01);
    for (i=0;i<16;i++)
    {
         CntRes0[i] += TS_CNTRES(i);
         printf("%2d ",CntRes0[i]);
     }
      delay_ms(1);
      printf("\n");
  
}
#endif


