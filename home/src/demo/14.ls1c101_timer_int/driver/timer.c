#include"Main.h"

//定时器中断
void timer_int(void)
{
	TIMER_CFG = 0;       //清零配置寄存器
	TIMER_CNT = 0;       //清零计数值
	TIMER_CMP = 8000000; //比较值设置为1s
	TIMER_STP = 16000000; //步进值也设置为2s
	TIMER_CFG = 0x07;    //开始计数,中断使能,周期触发
	INT_EN |= 0x01;      //使能中断
}
