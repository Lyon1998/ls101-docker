#ifndef MAIN_H_
#define MAIN_H_

typedef unsigned char       	INT8U;
typedef signed char				INT8S;
typedef unsigned short      	INT16U;
typedef signed short        	INT16S;
typedef unsigned int         	INT32U;//32位
typedef signed int           	INT32S;
typedef unsigned long       	INT64U;//64位
typedef signed long         	INT64S;
typedef float					FP32;
typedef float					FP64;
typedef unsigned char 		    BOOL;

#define NO_INIT 				volatile
#define OS_CLOCK_FREQ           8000000

#include"ls1c101.h"
#include"Common.h"
#include"Interrupt.h"
#include"gpio.h"
#include"delay.h"
#include"uart.h"
#include"clock.h"
#include"printf.h"
#include"rtc.h"
#include"adc.h"
#include"str_other.h"

typedef struct
{
	INT32U	c32k_trim	:4;	
	INT32U	c32k_speed 	:1;    		 
	INT32U	c32k_sel	:1;
	INT32U	osc8m_en	:1;
	INT32U	c8m_sel 	:1; 	
	INT32U	clkup_dly	:2;      
	INT32U	input_hold	:1;   	
	INT32U	fast_ram	:1;
	INT32U	tsensor_off	:1;
	INT32U	rtc_off		:1;
	INT32U	uart2_off	:1;
	INT32U	dram_pd		:1;
	INT32U	adc_on		:1;
	INT32U	adci0_ien	:1;
	INT32U	adci_pd		:1;
	INT32U	adci_pu		:1;
	INT32U	adc_en	    :4;
	INT32U	batdet_sel	:2;
	INT32U	spi_start	:1;
	INT32U	compact_mem	:1;
	INT32U	soft_flag   :4;
}PMU_ChipCtrlStruct;

#define spPMU_ChipCtrl         ((volatile PMU_ChipCtrlStruct *)(PMU_BASEADDR))


#ifdef  GLOBAL

///////////////////////////////////////////////////////////////////////////////
//////////////////////////////  以下定义数据表格  /////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void (* const SYS_IrqHandle[])(void) = {
		SOFT_INT,
        TIMER_8M_INT,
        TIMER_WAKE_INT,
        TOUCH,
        UART2_INT,
        BAT_FAIL,
        INTC,
        RING
};
#endif

#ifdef GLOBAL
#define EXT
#else
#define EXT  extern
#endif

EXT void	 (* const SYS_IrqHandle[])(void);



#endif


