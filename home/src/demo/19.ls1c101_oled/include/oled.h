#ifndef _OLED_H_
#define _OLED_H_

#define	DC_PIN				   			GPIO_PIN_16 //OLED数据/命令选择
#define	RES_PIN				  			GPIO_PIN_15 //OLED复位管脚

#define	LSLCD_INVERT			        0x1
#define Dis_X_MAX				        131	
#define Dis_Y_MAX				        63	
#define	COL_ROW_CONF			        1	//列行方式
//#define	COL_COL_CONF		        0	//逐列方式

#define ele_num(ele)                    (sizeof(ele) / sizeof(struct Element))
#define EN_CHAR_WID  	                8
#define CN_CHAR_WID                     16
#define CHAR_HEIG                       16
#define ONE_LINE_HEIG                   16
#define FIRST_LINE   	                0
#define SECOND_LINE  	                (16 * 1)
#define THIRD_LINE   	                (16 * 2)
#define FOUR_LINE    	                (16 * 3)

struct Element {
    int x;                      /*xpos*/
    int y;                      /*ypos*/
    //INT8U *text;				/*show text*/
    void *text;				    /*show text*/
};

struct WindowMenu {
    int ele_num;				/*The total of element*/
    struct Element *ele;		/**/
};

void		LCD_SPI_Init(void);
void		SSD1305_Init(void);
void        Lcd_Go_Sleep(void);
void		Fill_RAM(const INT8U Data);
void		FontSet(const INT8U Font_NUM, const INT8U Color);
void		PutString_I2C(const int x, const int y, const INT8U *p, int len, const INT8U mode);
void		clean_message(const char startrow, const char endrow);
void 		Chinese_show_test(void);
void 		English_show_test(void);
#endif
