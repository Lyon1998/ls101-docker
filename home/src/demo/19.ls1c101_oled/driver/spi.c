#include"Main.h"

void SPI_INIT(void)
{	
	gpio_pin_remap(GPIO_PIN_53,GPIO_FUNC_MAIN);
	gpio_pin_remap(GPIO_PIN_54,GPIO_FUNC_MAIN);
	gpio_pin_remap(GPIO_PIN_55,GPIO_FUNC_MAIN);
	gpio_pin_remap(GPIO_PIN_56,GPIO_FUNC_MAIN);
	
	SPI_SPCR = 0x00;                           
	SPI_SPSR = 0xc0;                          
	SPI_SPER = 0x00;                         
	SPI_SFC_PARAM = 0x01;                   
	SPI_SFC_SOFTCS = 0xff;                   
	SPI_SFC_TIMING = 0x00;                
	SPI_SPCR = 0x50;                           
	delay_ms(1);             						                                                  
}


