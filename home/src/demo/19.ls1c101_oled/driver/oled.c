#include"Main.h"
#include"font_lib.h"
static volatile unsigned char X_Witch = 0;		//字符写入时的宽度
static volatile unsigned char Y_Witch = 0;		//字符写入时的高度
static volatile int Font_Wrod 	= 0;			//字体的每个字模占用多少个存储单元数
static volatile int Char_Color 	= 0;

static void Write_Command(unsigned char Data)
{
	gpio_write_pin(DC_PIN,GPIO_LOW);
	while(!(SPI_SPSR & 0x04));
	SPI_SFC_SOFTCS = 0xBf;           //CSn2 拉低
	SPI_TxFIFO = Data;
	while((SPI_SPSR & 0x01));
	SPI_SFC_SOFTCS = 0xff;           //CSn2 拉高
	Data = SPI_TxFIFO;
}

static void Write_Data(unsigned char Data)
{
	gpio_write_pin(DC_PIN,GPIO_HIGH);
	while(!(SPI_SPSR & 0x04));
	SPI_SFC_SOFTCS = 0xBf;  
	SPI_TxFIFO = Data;
	while((SPI_SPSR & 0x01));
	SPI_SFC_SOFTCS = 0xff;
	Data = SPI_TxFIFO;
}


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//  Instruction Setting
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
static void inline Set_Start_Column(unsigned char d)
{
	Write_Command(0x00+d%16);	// Set Lower Column Start Address for Page Addressing Mode
								// Default => 0x00
	Write_Command(0x10+d/16);	// Set Higher Column Start Address for Page Addressing Mode
								// Default => 0x10
}

static void inline Set_Start_Page(unsigned char d)
{
	Write_Command(0xB0|d);	// Set Page Start Address for Page Addressing Mode
								// Default => 0xB0 (0x00)
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//  Show Regular Pattern (Full Screen)
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void Fill_RAM(const unsigned char Data)
{
	unsigned char i,j;

	for(i=0;i<8;i++)
	{
		Set_Start_Page(i);
		Set_Start_Column(0x02);

		for(j=0;j<128;j++)
		{
			Write_Data(Data);
		}
	}
}
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//  Initialization GPIO
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void LCD_SPI_Init(void)
{
	gpio_pin_remap(GPIO_PIN_53,GPIO_FUNC_MAIN);
	gpio_pin_remap(GPIO_PIN_54,GPIO_FUNC_MAIN);
	gpio_pin_remap(GPIO_PIN_55,GPIO_FUNC_MAIN);
	gpio_pin_remap(GPIO_PIN_57,GPIO_FUNC_MAIN);
	delay_ms(1);
	
	SPI_SPCR = 0x00;
    SPI_SPSR = 0x40;
    SPI_SPER = 0x00;
    SPI_SFC_PARAM = 0x01;
    SPI_SFC_SOFTCS = 0xff;
    SPI_SFC_TIMING = 0x0;
    SPI_SPCR = 0x51;
}

static void LCD_GPIO_Init(void)
{
	gpio_pin_remap(GPIO_PIN_15,GPIO_FUNC_MAIN);
	gpio_pin_remap(GPIO_PIN_16,GPIO_FUNC_MAIN);
	gpio_write_pin(RES_PIN,GPIO_LOW);
	LCD_SPI_Init();
	delay_ms(10);
	gpio_write_pin(RES_PIN,GPIO_HIGH);
}

//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//  Initialization
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void SSD1305_Init(void)
{
	printf("ssd1305_init start\n\r");	
	
	LCD_GPIO_Init();

	delay_ms(1);
	Write_Command(0xAE);     //Set Display Off 
    Write_Command(0xD5);     //display divide ratio/osc. freq. mode	
    Write_Command(0x80);     // 
    Write_Command(0xA8);     //multiplex ration mode:63 
    Write_Command(0x3F);
    Write_Command(0xD3);     //Set Display Offset   
    Write_Command(0x00);   
    Write_Command(0x40);     //Set Display Start Line  
    Write_Command(0x8D);     //DC-DC Control Mode Set 
    Write_Command(0x14);     //DC-DC ON/OFF Mode Set 
    Write_Command(0x20);     //Set Page Addressing Mode
    Write_Command(0x02);     // 
    Write_Command(0xA1);     //Segment Remap	 
    Write_Command(0xC8);     //Sst COM Output Scan Direction	
    Write_Command(0xDA);     //common pads hardware: alternative	
    Write_Command(0x12);
    Write_Command(0x81);     //contrast control 
    Write_Command(0xCF);     /////0X50 ff
    Write_Command(0xD9);	 //set pre-charge period	  
    Write_Command(0xF1);     /////0x1F 22
    Write_Command(0xDB);     //VCOM deselect level mode 
    Write_Command(0x30);	 //////40 18
    Write_Command(0xA4);     //Set Entire Display On/Off	
    Write_Command(0xA6);     //Set Normal Display 
	clean_message(0,3);
    Write_Command(0xAF);     //Set Display On
	
	delay_ms(100);
	printf("ssd1305_init end\n\r");	 
}

void Lcd_Go_Sleep(void)
{
	LCD_GPIO_Init();         //spi初始化
	delay_ms(1);
	Write_Command(0xAE);     //display off
	Write_Command(0x8D);     //set Charge Pump0x8D,0x10
	Write_Command(0x10); 
}

static void Write_8bit_data(int x, int y, unsigned char data)
{
	int x_low,x_hight;						//定义列地址的高低位指令
	int y_Page;								//用于存放要画点的位置所在的byte数据位置
	//x = x + 2;			//xpj修改
	//x = x + 1;			//xpj

	x_low = (x & 0x0F);						//定位列地址设置的低位指令
	x_hight = ((x >> 4) & 0x0F) + 0x10;		//定位列地址设置的高位指令
	y_Page = (y >> 3) + 0xB0;				//Get the page of the byte
	Write_Command(y_Page);
	Write_Command(x_low);
	Write_Command(x_hight);
	Write_Data(data);
}

//========================================================================
// 函数: void FontSet(unsigned char Font_NUM, unsigned char Color)
// 描述: 文本字体设置
// 参数: Font_NUM 字体选择,以驱动所带的字库为准
//	Color  文本颜色,仅作用于自带字库
// 返回: 无
// 备注: 
// 版本:
//========================================================================
void FontSet(const unsigned char Font_NUM, const unsigned char Color)
{
	printf("----chinese4-----\n");
        switch(Font_NUM)
        {
				case 0:         Font_Wrod = 10;	//ASII字符 小号字体  此模式提供给状态栏显示时间等信息
                                X_Witch = 6;
                                Y_Witch = 10;
                                Char_Color = Color;
                    break;
                case 1:         Font_Wrod = 16;	//ASII字符 大号字体
                                X_Witch = 8;
                                Y_Witch = 16;
                                Char_Color = Color;
                    break;
                case 2:         Font_Wrod = 32;	//汉字A
                                X_Witch = 16;
                                Y_Witch = 16;
                                Char_Color = Color;
                    break;
                default:
                    break;
        }
}

static void PutChar_I2C(int x, int y, const unsigned char *buf, unsigned char mode)
{
	int i,j;		//数据暂存
	const unsigned char *p_data;
	unsigned char Temp = 0;
	int Index = 0;

	p_data = buf;

//#ifdef	COL_COL_CONF
#if 0
	int k;
	i = 0;
	k = y;
	while(i < X_Witch)
	{
		j = 0;
		y = k;
		while(j < Y_Witch)
		{
			if(y > Dis_Y_MAX) break;
			Temp = *(p_data+Index);
			if ((mode & LSLCD_INVERT) == LSLCD_INVERT)
				Temp = ~Temp;
			Index++;

			Write_8bit_data(x+i, y, Temp);
			if((x+i) >= Dis_X_MAX)
			{
				Index += (X_Witch-i)>>3;
				break;
			}
			j += 8;
			y += 8;
		}
		i++;
	}
#else
	j = 0;
	while(j < Y_Witch)
	{
		if(y > Dis_Y_MAX) break;
		i = 0;
		while(i < X_Witch)
		{
			Temp = *(p_data+Index);
			if ((mode & LSLCD_INVERT) == LSLCD_INVERT)
				Temp = ~Temp;
			Index++;

			Write_8bit_data(x+i, y, Temp);
			if((x+i) >= Dis_X_MAX)
			{
				Index += (X_Witch-i)>>3;
				break;
			}
			i++;
		}
		y += 8;
		j += 8;
	}
#endif
}

void PutString_I2C(const int x, const int y, const unsigned char *p, int len, const unsigned char mode)
{
	printf("----chinese5-----\n");
	int i,j;
	unsigned char k;
	i = x;
	j = y;
	k = mode;
	while(len-- != 0)
	{
		PutChar_I2C(i, j, p, k);
		i += X_Witch;
		if((i + X_Witch) > Dis_X_MAX)
		{
			i = 0;
			if((Dis_Y_MAX - j) < Y_Witch) break;
			else j += Y_Witch;
		}
		p += Font_Wrod;
	}
}


/*
清屏函数；
参数： startrow---开始行；
      endrow---结束行；
*/
void clean_message(const char startrow, const char endrow)
{
    int i,j;
    unsigned char *text = (unsigned char *)(" ");
    if(startrow == 0 && endrow == 3)
	{
        Fill_RAM(0x00);
	}
}


void English_show_test(void)
{
	INT8U english1[] = {
		0x08,0x38,0xC8,0x00,0xC8,0x38,0x08,0x00,
        0x00,0x00,0x20,0x3F,0x20,0x00,0x00,0x00};
	INT8U english2[] = {
		0x08,0xF8,0xF8,0x00,0xF8,0xF8,0x08,0x00,
        0x20,0x3F,0x00,0x3F,0x00,0x3F,0x20,0x00};
    
	X_Witch = 8;
    Y_Witch = 16;
    PutChar_I2C(8,0,english2,0);
	PutChar_I2C(16,0,english1,0);
}

void Chinese_show_test(void)
{
	INT8U chinese1[] = {
		0x10,0x10,0x10,0x10,0x10,0xFF,0x10,0x10,
		0xF0,0x10,0x11,0x16,0xD0,0x10,0x10,0x00,
		0x80,0x40,0x20,0x18,0x06,0x41,0x20,0x10,
		0x3F,0x44,0x42,0x41,0x40,0x40,0x78,0x00
	};
	
	INT8U chinese2[] = {
		0x04,0x04,0x04,0x04,0x1F,0x04,0x24,0x44,
        0x84,0x04,0x1F,0x04,0x04,0x04,0x04,0x00,
        0x10,0x08,0x06,0x00,0x00,0x3F,0x40,0x40,
        0x40,0x40,0x40,0x70,0x01,0x02,0x0C,0x00
	};
    
	X_Witch = 16;
    Y_Witch = 16;
	PutChar_I2C(32,0,chinese1,0);
	PutChar_I2C(48,0,chinese2,0);
	
}

