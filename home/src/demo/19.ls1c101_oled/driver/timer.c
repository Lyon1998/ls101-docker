#include"Main.h"

//定时器中断
void timer_int(void)
{
	TIMER_CFG = 0;       //清零配置寄存器
	TIMER_CNT = 0;       //清零计数值
	TIMER_CMP = 8000000; //比较值设置为1s
	TIMER_STP = 16000000; //步进值也设置为2s
	TIMER_CFG = 0x07;    //开始计数,中断使能,周期触发
	INT_EN |= 0x01;      //使能中断
}

void rtc_timer_count(INT32U settime,INT32U systime)
{
	NO_INIT INT32U time = 0;
	
	time = (PMU_Count & 0xfffff);

	if(time < systime)
	{
		time += 0x100000;
	}

	if((time - systime) > (256*settime))  			//RTC时间计数器每1/256s加一次
	{
		printf("-------timer handle-------\n");	
		get_current_timer = (PMU_Count & 0xfffff);  //重新开锁定时
	}
	else
	{
		printf("-------sys running-------\n");		
		delay_s(1);
	}
}
		
/***********************************************************************
函数功能:         定时函数,此函数
输入参数:         time 等于0时，表示关闭定时
输出参数:         无
函数返回值说明:
使用的资源:    	  无
***********************************************************************/
void rtc_wake_set(INT32U time)
{
	if(time != 0)
	{
		INT32U compare = 0;
		compare = (PMU_Count & 0xfffff) + (time*256);
		PMU_Compare  = compare;                 //当PMU_Count计数值与该寄存器中的值相同时,产生
		PMU_CmdSts |= (1<<7)|(1<<8);            //打开定时唤醒使能，中断使能唤醒系统中断
	}
	else
	{
		PMU_CmdSts &=  ~((1<<7)|(1<<8));        //禁止使能
	}
}

	
