#include"Main.h"
#include"voice.h"

void vpwm_gpio_set(void)
{
	gpio_pin_remap(GPIO_PIN_61,GPIO_FUNC_MAIN);
	gpio_pin_remap(GPIO_PIN_62,GPIO_FUNC_MAIN);
	gpio_pin_remap(GPIO_PIN_44,GPIO_FUNC_MAIN);
	gpio_pin_remap(GPIO_PIN_45,GPIO_FUNC_MAIN);
	gpio_pin_remap(GPIO_PIN_46,GPIO_FUNC_MAIN);
	gpio_pin_remap(GPIO_PIN_47,GPIO_FUNC_MAIN);
	gpio_pin_remap(GPIO_PIN_48,GPIO_FUNC_MAIN);
	
	gpio_write_pin(POWER_VOICE,GPIO_HIGH);
}

void vpwm_init_dma_bd(void)
{
#if 0                     //外部8M
	VPWM_CFG=0x48bb1f41; 
#else                     //内部主频
	INT32U sum_width;
    INT32U cfg;
    INT32U freq = (*(volatile unsigned int *)0xbf0201b0)*1000;
	
	freq <<= 2;          								// busclk * 4 = osc clk
	unsigned int rate = 16000 * 2;
	sum_width = (freq+rate-1) / rate;
    cfg = 0x41ab0000 | (sum_width << 4);                  // bit[27]  0: 32m 1: busclk
    VPWM_CFG = cfg;
#endif      
}

//DMA复位
void dma_reset(void)   
{
    unsigned int status;
    DMA_CMD_STATUS = 0x80000000;    //DMA控制器写1复位
    status = DMA_CMD_STATUS;        //读DMA控制器，为0则表示软复位完成
    while (status != 0x1)
        status = DMA_CMD_STATUS;
}


void dma_start(INT32U *source, INT32U count, INT32U mode)
{
    unsigned int status;
    status = DMA_CMD_STATUS;
    while(status != 0x1)            //命令可写
		status = DMA_CMD_STATUS;
    
	DMA_SOURCE = ((unsigned int)source)&0x00ffffff;
    DMA_COUNT  = count;
    
    if(mode == 0x1)
       DMA_CMD_STATUS = 0x3;       //使能DMA
	else
       DMA_CMD_STATUS = 0x1;
}

void buzzer_warn(INT32U audio_num)
{
	wdt_dog_feed();      //喂狗
	
	vpwm_init_dma_bd(); 
	Audio_TypeDef sound_name = {audio_pos[audio_num], audio_pos[audio_num+1]-audio_pos[audio_num]};
	printf("\n%d %d", sound_name.index, sound_name.size);   	
	dma_start(AUDIO_DATA+sound_name.index * 4, sound_name.size, 0x1);
}  

