#include"Main.h"

void LCD_SPI_Init(void)
{
	gpio_pin_remap(GPIO_PIN_53,GPIO_FUNC_MAIN);
	gpio_pin_remap(GPIO_PIN_54,GPIO_FUNC_MAIN);
	gpio_pin_remap(GPIO_PIN_55,GPIO_FUNC_MAIN);
	gpio_pin_remap(GPIO_PIN_57,GPIO_FUNC_MAIN);
	
	SPI_SPCR = 0x00;
    SPI_SPSR = 0x40;
    SPI_SPER = 0x00;
    SPI_SFC_PARAM = 0x01;
    SPI_SFC_SOFTCS = 0xff;
    SPI_SFC_TIMING = 0x0;
    SPI_SPCR = 0x51;
    delay_ms(1);
}

void Write_Command(unsigned char Data)
{
	gpio_write_pin(DC_PIN,GPIO_LOW);
	while(!(SPI_SPSR & 0x04));
	SPI_SFC_SOFTCS = 0xBf;           //CSn2 ����
	SPI_TxFIFO = Data;
	while((SPI_SPSR & 0x01));
	SPI_SFC_SOFTCS = 0xff;           //CSn2 ����
	Data = SPI_TxFIFO;
}

void Write_Data(unsigned char Data)
{
	gpio_write_pin(DC_PIN,GPIO_HIGH);
	while(!(SPI_SPSR & 0x04));
	SPI_SFC_SOFTCS = 0xBf;  
	SPI_TxFIFO = Data;
	while((SPI_SPSR & 0x01));
	SPI_SFC_SOFTCS = 0xff;
	Data = SPI_TxFIFO;
}

void SPI_INIT(void)
{	
	gpio_pin_remap(GPIO_PIN_53,GPIO_FUNC_MAIN);
	gpio_pin_remap(GPIO_PIN_54,GPIO_FUNC_MAIN);
	gpio_pin_remap(GPIO_PIN_55,GPIO_FUNC_MAIN);
	gpio_pin_remap(GPIO_PIN_56,GPIO_FUNC_MAIN);
	
	SPI_SPCR = 0x00;                           
	SPI_SPSR = 0xc0;                          
	SPI_SPER = 0x00;                         
	SPI_SFC_PARAM = 0x01;                   
	SPI_SFC_SOFTCS = 0xff;                   
	SPI_SFC_TIMING = 0x00;                
	SPI_SPCR = 0x50;                           
	delay_ms(1);             						                                                  
}


