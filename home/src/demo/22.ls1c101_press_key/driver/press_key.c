#include"Main.h"

void press_key_init(void)
{
	gpio_pin_remap(GPIO_PIN_17,GPIO_FUNC_MAIN);    
}

void press_key_scanning(void)
{
	press_key_init();
	
	if(gpio_read_pin(GPIO_PIN_17) == 0)
	{
		delay_ms(20);
		if(gpio_read_pin(GPIO_PIN_17) == 0)
		{
			buzzer_warn(1,0);
			delay_s(2);
			printf("------press key test--------\n");
		}
	}
}
