#ifndef _SPI_H_
#define _SPI_H_

void LCD_SPI_Init(void);
void Write_Command(unsigned char Data);
void Write_Data(unsigned char Data);
void SPI_INIT(void);

#endif
