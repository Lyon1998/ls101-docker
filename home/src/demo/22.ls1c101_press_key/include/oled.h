#ifndef _OLED_H_
#define _OLED_H_

#define	DC_PIN				   GPIO_PIN_17 //OLED数据/命令选择
#define	RES_PIN				   GPIO_PIN_16 //OLED复位管脚

#define	LSLCD_INVERT			        0x1
#define Dis_X_MAX				        131	
#define Dis_Y_MAX				        63	
#define	COL_ROW_CONF			        1	//列行方式
//#define	COL_COL_CONF		        0	//逐列方式

#define ele_num(ele)                   (sizeof(ele) / sizeof(struct Element))
#define EN_CHAR_WID  	                8
#define CN_CHAR_WID                     16
#define CHAR_HEIG                       16
#define ONE_LINE_HEIG                   16
#define FIRST_LINE   	                0
#define SECOND_LINE  	                (16 * 1)
#define THIRD_LINE   	                (16 * 2)
#define FOUR_LINE    	                (16 * 3)

struct Element {
    int x;                      /*xpos*/
    int y;                      /*ypos*/
    //INT8U *text;				/*show text*/
    void *text;				    /*show text*/
};

struct WindowMenu {
    int ele_num;				/*The total of element*/
    struct Element *ele;		/**/
};

///////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////  界面显示  ////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
#define    UI_PAGE_WELCOME              0	//欢迎使用 龙芯智能锁
#define    UI_PAGE_MODE_LOGIN		    1	//进入设置 输入管理员密码
#define    UI_PAGE_SETTING_LIST         2	//设置列表
#define    UI_PAGE_SETTING_USER_LIST	3	//用户列表 添加 删除 清空 设置管理员
#define    UI_PAGE_USER_ADD_LIST		4	//添加的用户列表
#define    UI_PAGE_USER_ADD_VERIFY		5	//确认输入的密码
#define    UI_PAGE_ADD_USER_FINGER		6	//拿开手指，在按一次
#define    UI_PAGE_ADD_SUCCESS			7	//添加成功 编号
#define    UI_PAGE_ADD_FAIL				8	//添加失败
#define    UI_PAGE_USER_EXSIT			9	//已存在,添加其他类型用户
//#define    UI_PAGE_USER_DEL_LIST		10	//删除用户列表
#define    UI_PAGE_DELETE_FAIL			11	//删除失败
#define    UI_PAGE_DELETE_SUCCESS		12	//删除成功
#define    UI_PAGE_ADMIN_PASSWD			13	//输入管理员新密码
#define    UI_PAGE_REVISE_FAIL			14	//修改失败
#define    UI_PAGE_REVISE_SUCCESS		15	//修改成功
#define    UI_PAGE_SETTING_INFO_LIST	16	//信息列表
#define    UI_PAGE_SHOW_LOG				17	//查看日志格式
#define    UI_PAGE_DEL_LOG_NUM			18	//输入删除日志ID
#define    UI_PAGE_SETTING_INFO_USER	19	//查看用户信息
#define    UI_PAGE_SETTING_SYSTEM		20	//系统列表
#define    UI_PAGE_VERIFY_MODE_LIST		21	//开锁方式列表
#define    UI_MANAGE_PASSWD_AGAIN		22	//确认输入的密码	管理员使用
#define    UI_PAGE_VERSION				23	//版本信息
#define    UI_PAGE_SETTING_TIME			24	//时间设置格式 
#define    UI_PAGE_SET_SUCCESS			25	//设置成功
#define    UI_PAGE_SET_FAIL				26	//设置失败
#define    UI_PAGE_RESTORE_VERIFY		27	//恢复出厂
#define    UI_PAGE_OPEN_FAIL			28	//待机2分钟
#define    UI_PAGE_FG_SUCCESS_PA		29	//指纹成功 输入密码
#define    UI_PAGE_PA_SUCCESS_FG		30	//密码成功 请放手指
#define    UI_PAGE_CA_SUCCESS_PA		31	//磁卡成功 输入密码
#define    UI_PAGE_PA_SUCCESS_CA		32	//密码成功 请刷磁卡
#define    UI_PAGE_POWER_ALARM			33	//电量低 请换电池
#define    UI_PAGE_INPUT_PASSWORD		34	//龙芯智能锁
#define    UI_PAGE_CLEAR_ALL			35	//已清空
#define    UI_PAGE_RESTORE_VERIFY_FA	36	//恢复出厂失败
#define    UI_PAGE_OPEN_SU				37	//开锁成功
#define    UI_PAGE_UNOPEN_SU			38	//门已上锁
#define    UI_PAGE_OPEN_FA				39	//开锁失败
#define    UI_PAGE_ADD_PASSWORD			40	//添加密码
#define    UI_PAGE_ADD_FINGER			41	//添加指纹
#define    UI_PAGE_ADD_CARD				42	//添加卡片
#define    UI_PAGE_DEL_PASSWORD_NUM		43	//编号删除密码
#define    UI_PAGE_DEL_FINGER_NUM		44	//编号删除指纹
#define    UI_PAGE_DEL_CARD_NUM			45	//编号删除卡片
#define    UI_PAGE_INFO_MANAGE_LIST     46  //日志管理
#define    UI_PAGE_MEMOMRY_INFO_LIST    47  //内存信息
#define    UI_PAGE_CLEAR_ALL_LOG        48  //清空日志
#define    UI_PAGE_SETTING_INFO_MANAGE  49  //有效管理员界面
#define    UI_PAGE_SETTING_SECURITY     50  //安全设置
#define    UI_PAGE_SETTING_NORMAL_OPEN  51  //常开设置列表
#define    UI_PAGE_VOICE_SWITCH         52  //语音开关
#define    UI_PAGE_ADD_ADMIN_LIST       53	//添加管理
#define    UI_PAGE_MANAGE_ADD_LIST      54  //增加管理员
#define    UI_PAGE_DEL_ADMIN_LIST		55	//删除管理
#define    UI_PAGE_MANAGE_DEL_LIST      56  //删除管理员
#define    UI_PAGE_PASSWD_DEL_MODE      57  //密码删除方式
#define    UI_PAGE_FINGER_DEL_MODE      58  //指纹删除方式
#define    UI_PAGE_CARD_DEL_MODE        59  //卡片删除方式
#define    UI_PAGE_DEL_PASSWORD_VREIFY  60  //验证删除密码
#define    UI_PAGE_DEL_PASSWORD_EMPTY   61  //清空密码
#define    UI_PAGE_DEL_FINGER_VREIFY    62  //指纹验证删除
#define    UI_PAGE_DEL_FINGER_EMPTY     63  //清空指纹
#define    UI_PAGE_DEL_CARD_VREIFY      64  //卡片验证删除
#define    UI_PAGE_DEL_CARD_EMPTY       65  //清空卡片
#define    UI_PAFE_OPEN_DOOR_SET        66  //常开提示
#define    UI_PAGE_PASSWD_FULL          67  //密码已存满
#define    UI_PAGE_FINGER_FULL          68  //指纹已存满
#define    UI_PAGE_CARD_FULL            69  //卡片已存满
#define    UI_PAGE_ClEAR_PASSWD_USER    70  //清除全部用户密码
#define    UI_PAGE_ClEAR_PASSWD_MANAGE  71  //清除全部管理员密码
#define    UI_PAGE_ClEAR_FINGER_USER    72  //清除全部用户指纹
#define    UI_PAGE_ClEAR_FINGER_MANAGE  73  //清除全部管理员指纹
#define    UI_PAGE_ClEAR_CARD_USER      74  //清除全部用户卡片
#define    UI_PAGE_ClEAR_CARD_MANAGE    75  //清除全部管理员卡片
#define    UI_PAGE_IN_RECOVERY          76  //恢复出厂设置中


void Power_Frame(void);
void PutString_I2C(const int x, const int y, const unsigned char *p, int len, const unsigned char mode);
void PutChar_I2C(int x, int y, const unsigned char *buf, unsigned char mode);
void FontSet(const unsigned char Font_NUM, const unsigned char Color);
void inline Set_Start_Column(unsigned char d);
void inline Set_Start_Page(unsigned char d);
void Fill_RAM(const unsigned char Data);
void LCD_GPIO_Init(void);
void SSD1305_Init(void);
void OLED_Refresh_Gram(void);
void Lcd_Go_Sleep(void); 
void Write_8bit_data(int x, int y, unsigned char data);

void drawPixel(const int x, const int y, const INT8U *mar, const INT8U mode, const INT8U word_type);
int searchCNCode(const PUnicode unicode, const int code);
int searchENCode(const PAscii ascii_p, const INT8U code);
void showChineseChar(const int x, const int y, const int code, const INT8U mode);
void showEnglishChar(const int x, const int y, const INT8U code, const INT8U mode);
void showString(const int x, const int y, INT8U *str, const INT8U mode);
void clean_message(const char startrow, const char endrow);

#endif
