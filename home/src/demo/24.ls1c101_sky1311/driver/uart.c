#include"Main.h"

/*
 *   串口波特率配置
 *   base_addr： 需要配置的串口基址
 *   frep     :  主频
 *   baud_rate： 波特率
 */
void uart_cfg_div(volatile INT8U* base_addr, INT32U freq, INT32U baud_rate)
{
    INT8U lcr = base_addr[3];
    INT32U div=0;
    
    div = ((freq << 8) >> 4) / baud_rate;      //[23:16] DL_H  [15: 8] DL_L  [ 7: 0] DL_D
    
    base_addr[3] = 0x80;
    base_addr[2] = div & 0xff;
    div >>= 8;
    base_addr[0] = div & 0xff;
    div >>= 8;
    base_addr[1] = div & 0xff;
    base_addr[3] = lcr; // restore lcr
}

void uart0_init(void)
{
	gpio_pin_remap(GPIO_PIN_6,GPIO_FUNC_MAIN);          //管脚复用为RX
	gpio_pin_remap(GPIO_PIN_7,GPIO_FUNC_MAIN);          //管脚复用为TX
	
	Uart0_FCR = FIFO_TRIGGER_1 | FIFO_TX_RST | FIFO_RX_RST; 
    Uart0_LCR = CLCR_DLAB;      //Baud rate set as 115200
	uart_cfg_div(UART0_BASEADDR,OS_CLOCK_FREQ,115200);
    Uart0_LCR = CLCR_8BITS; 
    Uart0_IER = 0x0;

}

void uart0_send(INT8U str)
{
	while (!(Uart0_LSR & 0x20));
    Uart0_TxData = str;
}


