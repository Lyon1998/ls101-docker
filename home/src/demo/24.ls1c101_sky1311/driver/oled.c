#include"Main.h"

static volatile unsigned char X_Witch = 0;		//字符写入时的宽度
static volatile unsigned char Y_Witch = 0;		//字符写入时的高度
static volatile int Font_Wrod 	= 0;			//字体的每个字模占用多少个存储单元数
static volatile int Char_Color 	= 0;

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//  Instruction Setting
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void inline Set_Start_Column(INT8U d)
{
	Write_Command(0x00+d%16);	// Set Lower Column Start Address for Page Addressing Mode
								// Default => 0x00
	Write_Command(0x10+d/16);	// Set Higher Column Start Address for Page Addressing Mode
								// Default => 0x10
}

void inline Set_Start_Page(INT8U d)
{
	Write_Command(0xB0|d);	// Set Page Start Address for Page Addressing Mode
								// Default => 0xB0 (0x00)
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//  Show Regular Pattern (Full Screen)
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void Fill_RAM(const INT8U Data)
{
	unsigned char i,j;

	for(i=0;i<8;i++)
	{
		Set_Start_Page(i);
		Set_Start_Column(0x02);

		for(j=0;j<128;j++)
		{
			Write_Data(Data);
		}
	}
}

void LCD_GPIO_Init(void)
{
	gpio_write_pin(RES_PIN,GPIO_LOW);
	LCD_SPI_Init();
	delay_ms(10);
	gpio_write_pin(RES_PIN,GPIO_HIGH);
}

//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//  Initialization
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void SSD1305_Init(void)
{
	printf("ssd1305_init start\n\r");	
	
	LCD_GPIO_Init();

	delay_ms(1);

	/*
	//SH1106 init *
	Write_Command(0xAE);     //Set Display Off 
    Write_Command(0xD5);     //display divide ratio/osc. freq. mode	
    Write_Command(0x80);     // 
    Write_Command(0xA8);     //multiplex ration mode:63 
    Write_Command(0x3F);
    Write_Command(0xD3);     //Set Display Offset   
    Write_Command(0x00);   
    Write_Command(0x40);     //Set Display Start Line  
    Write_Command(0xAD);     //DC-DC Control Mode Set 
    Write_Command(0x8B);     //DC-DC ON/OFF Mode Set 
    Write_Command(0x32);     //Set Pump voltage value 
    Write_Command(0xA1);     //Segment Remap	 
    Write_Command(0xC8);     //Sst COM Output Scan Direction	
    Write_Command(0xDA);     //common pads hardware: alternative	
    Write_Command(0x12);
    Write_Command(0x81);     //contrast control 
    Write_Command(0x50);     /////0X50 ff
    Write_Command(0xD9);	 //set pre-charge period	  
    Write_Command(0x1F);     /////0x1F 22
    Write_Command(0xDB);     //VCOM deselect level mode 
    Write_Command(0x40);	 //////40 18
    Write_Command(0xA4);     //Set Entire Display On/Off	
    Write_Command(0xA6);     //Set Normal Display 
	clean_message(0,3);
    Write_Command(0xAF);     //Set Display On
	*/	
	/*
	Write_Command(0xAE);     //Set Display Off 
    Write_Command(0x00);     //display divide ratio/osc. freq. mode	
    Write_Command(0x10);     // 
    Write_Command(0x40);     //multiplex ration mode:63 
    Write_Command(0x81);
    Write_Command(0xcf);     //Set Display Offset   
    Write_Command(0xa1);   
    Write_Command(0xc8);     //Set Display Start Line  
    Write_Command(0xA6);     //DC-DC Control Mode Set 
    Write_Command(0xA8);     //DC-DC ON/OFF Mode Set 
    Write_Command(0x3F);     //Set Pump voltage value 
    Write_Command(0xD3);     //Segment Remap	 
    Write_Command(0x00);     //Sst COM Output Scan Direction	
    Write_Command(0xD5);     //common pads hardware: alternative	
    Write_Command(0x80);
    Write_Command(0xD9);     //contrast control 
    Write_Command(0xF1);     /////0X50 ff
    Write_Command(0xDA);	 //set pre-charge period	  
    Write_Command(0x12);     /////0x1F 22
    Write_Command(0xDB);     //VCOM deselect level mode 
    Write_Command(0x40);	 //////40 18
    Write_Command(0x20);     //Set Entire Display On/Off	
    Write_Command(0x02);     //Set Normal Display 
    Write_Command(0x8D);     //
    Write_Command(0x14);     //
    Write_Command(0xA4);     //
    Write_Command(0xA6);     //
	clean_message(0,3);
    Write_Command(0xAF);     //Set Display On
	*/
	
	Write_Command(0xAE);     //Set Display Off 
    Write_Command(0xD5);     //display divide ratio/osc. freq. mode	
    Write_Command(0x80);     // 
    Write_Command(0xA8);     //multiplex ration mode:63 
    Write_Command(0x3F);
    Write_Command(0xD3);     //Set Display Offset   
    Write_Command(0x00);   
    Write_Command(0x40);     //Set Display Start Line  
    Write_Command(0x8D);     //DC-DC Control Mode Set 
    Write_Command(0x14);     //DC-DC ON/OFF Mode Set 
    Write_Command(0x20);     //Set Page Addressing Mode
    Write_Command(0x02);     // 
    Write_Command(0xA1);     //Segment Remap	 
    Write_Command(0xC8);     //Sst COM Output Scan Direction	
    Write_Command(0xDA);     //common pads hardware: alternative	
    Write_Command(0x12);
    Write_Command(0x81);     //contrast control 
    Write_Command(0xCF);     /////0X50 ff
    Write_Command(0xD9);	 //set pre-charge period	  
    Write_Command(0xF1);     /////0x1F 22
    Write_Command(0xDB);     //VCOM deselect level mode 
    Write_Command(0x30);	 //////40 18
    Write_Command(0xA4);     //Set Entire Display On/Off	
    Write_Command(0xA6);     //Set Normal Display 
	clean_message(0,3);
    Write_Command(0xAF);     //Set Display On
	
	delay_ms(100);
	printf("ssd1305_init end\n\r");	 
}

void Lcd_Go_Sleep(void)
{
	LCD_GPIO_Init();         //spi初始化
	delay_ms(1);
	Write_Command(0xAE);     //display off
	Write_Command(0x8D);     //set Charge Pump0x8D,0x10
	Write_Command(0x10); 
}

void Write_8bit_data(int x, int y, INT8U data)
{
	int x_low,x_hight;						//定义列地址的高低位指令
	int y_Page;								//用于存放要画点的位置所在的byte数据位置
	//x = x + 2;			//xpj修改
	//x = x + 1;			//xpj

	x_low = (x & 0x0F);						//定位列地址设置的低位指令
	x_hight = ((x >> 4) & 0x0F) + 0x10;		//定位列地址设置的高位指令
	y_Page = (y >> 3) + 0xB0;				//Get the page of the byte
	Write_Command(y_Page);
	Write_Command(x_low);
	Write_Command(x_hight);
	Write_Data(data);
}

//========================================================================
// 函数: void FontSet(unsigned char Font_NUM, unsigned char Color)
// 描述: 文本字体设置
// 参数: Font_NUM 字体选择,以驱动所带的字库为准
//	Color  文本颜色,仅作用于自带字库
// 返回: 无
// 备注: 
// 版本:
//========================================================================
void FontSet(const INT8U Font_NUM, const INT8U Color)
{
	switch(Font_NUM)
    {
		case 0:         
			Font_Wrod = 10;	//ASII字符 小号字体  此模式提供给状态栏显示时间等信息
            X_Witch = 6;
            Y_Witch = 10;
            Char_Color = Color;
        break;
        case 1:         
			Font_Wrod = 16;	//ASII字符 大号字体
            X_Witch = 8;
            Y_Witch = 16;
            Char_Color = Color;
        break;
        case 2:         
			Font_Wrod = 32;	//汉字A
            X_Witch = 16;
            Y_Witch = 16;
            Char_Color = Color;
        break;
        default:
            break;
    }
}

void PutChar_I2C(int x, int y, const INT8U *buf, INT8U mode)
{
	int i,j;		//数据暂存
	const unsigned char *p_data;
	unsigned char Temp = 0;
	int Index = 0;

	p_data = buf;

//#ifdef	COL_COL_CONF
#if 0
	int k;
	i = 0;
	k = y;
	while(i < X_Witch)
	{
		j = 0;
		y = k;
		while(j < Y_Witch)
		{
			if(y > Dis_Y_MAX) break;
			Temp = *(p_data+Index);
			if ((mode & LSLCD_INVERT) == LSLCD_INVERT)
				Temp = ~Temp;
			Index++;

			Write_8bit_data(x+i, y, Temp);
			if((x+i) >= Dis_X_MAX)
			{
				Index += (X_Witch-i)>>3;
				break;
			}
			j += 8;
			y += 8;
		}
		i++;
	}
#else
	j = 0;
	while(j < Y_Witch)
	{
		if(y > Dis_Y_MAX) break;
		i = 0;
		while(i < X_Witch)
		{
			Temp = *(p_data+Index);
			if ((mode & LSLCD_INVERT) == LSLCD_INVERT)
				Temp = ~Temp;
			Index++;

			Write_8bit_data(x+i, y, Temp);
			if((x+i) >= Dis_X_MAX)
			{
				Index += (X_Witch-i)>>3;
				break;
			}
			i++;
		}
		y += 8;
		j += 8;
	}
#endif
}

void PutString_I2C(const int x, const int y, const INT8U *p, int len, const INT8U mode)
{
	int i,j;
	unsigned char k;
	i = x;
	j = y;
	k = mode;
	while(len-- != 0)
	{
		PutChar_I2C(i, j, p, k);
		i += X_Witch;
		if((i + X_Witch) > Dis_X_MAX)
		{
			i = 0;
			if((Dis_Y_MAX - j) < Y_Witch) break;
			else j += Y_Witch;
		}
		p += Font_Wrod;
	}
}

void drawPixel(const int x, const int y, const INT8U *mar, const INT8U mode, const INT8U word_type)
{
    FontSet(word_type,1);
    PutString_I2C(x, y, mar, 1, mode);
}

/*Use binary search*/
int searchCNCode(const PUnicode unicode, const int code)
{
	int min = 0;
	int max = CHINESE_CHAR_NUM - 1;
	int mid;
	if (code < unicode[min].u_code || code > unicode[max].u_code) return -1;
	if (unicode[min].u_code == code) return min;
	if (unicode[max].u_code == code) return max;
    for(mid = min; mid <= max; mid++)
       if (unicode[mid].u_code == code)
           return mid;
	return -1;
}

/*Use binary search*/
int searchENCode(const PAscii ascii_p, const INT8U code)//避免与数组冲突
{
	int min = 0;
    int max = ASCII_CHAR_NUM - 1;
	int mid;
    if (code < ascii_p[min].u_code || code > ascii_p[max].u_code) return -1;
    if (ascii_p[min].u_code == code) return min;
    if (ascii_p[max].u_code == code) return max;

    for(mid = min; mid <= max; mid++)
    	if (ascii_p[mid].u_code == code)
               return mid;
	return -1;
}

/*Draw one Chinese char depend on struct Unicode which in font_lib.h*/
void showChineseChar(const int x, const int y, const int code, const INT8U mode)
{
    int index = searchCNCode((PUnicode)chinese, code);
	if (index < 0) return;
    drawPixel(x, y, chinese[index].data, mode, 2);	
}

/*Draw one Asicc char depend on struct Ascii which in font_lib.h*/
void showEnglishChar(const int x, const int y, const INT8U code, const INT8U mode)
{
    int index = searchENCode((PAscii)ascii, code);
	if (index < 0) return;
    drawPixel(x, y, ascii[index].data, mode, 1);
}

void showString(const int x, const int y, INT8U *str, const INT8U mode)
{
	int len = strlen((char *)str);
	if (len > 30) 
		return;
    unsigned char pos = x;
	unsigned char *p = str;	
	int i = 0;
	int code = 0;
//	ConverInt con;
	while (i < len) 
	{
		if (*(p + i) > 0xE0) 
		{
			/*Chinese char, Unicode*/
			code = (*(p + i) << 16) | (*(p + i + 1) << 8) | *(p + i + 2);
            showChineseChar(pos, y, code, mode);
            pos += CN_CHAR_WID;
			i +=3;
		} 
		else 
		{
			/*English char*/
            showEnglishChar(pos, y, *(p + i), mode);
            pos += EN_CHAR_WID;
            i++;
        }
	}
}

/*
清屏函数；
参数： startrow---开始行；
      endrow---结束行；
*/
void clean_message(const char startrow, const char endrow)
{
    int i,j;
    unsigned char *text = (unsigned char *)(" ");
    if(startrow == 0 && endrow == 3)
	{
        Fill_RAM(0x00);
	}
    else{
        for(i = startrow; i <= endrow; i++ )
            for(j = 0; j < 16; j++)
                showString(8 + 8 * j, 16 * i, text, 0);
    }
}

void Power_Frame(void)
{
	showString(0,0,"123456",1);
}


