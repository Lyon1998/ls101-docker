#ifndef _Interrupt_H_
#define _Interrupt_H_

void			SOFT_INT(void);
void			TIMER_8M_INT(void);
void			TIMER_WAKE_INT(void);
void			TOUCH(void);
void			UART2_INT(void); 
void			BAT_FAIL(void);
void			INTC(void);
void			RING(void);
void 			IRQ_Exception();

#endif
