#include"Main.h"

//外部8M时钟配置
void os_clock_init(void)
{
	// select ext8m
	PMU_ChipCtrl &= ~(1<<7);
	PMU_ChipCtrl |= (1<<6);
	
	delay_ms(10);     ////////////////////////不加延时，睡眠起来后会时钟失效
	while((PMU_CmdSts&(1<<31)) == 1)
	{
		PMU_ChipCtrl &= ~(1<<7);
	}
	PMU_ChipCtrl|=(1<<7);
}

