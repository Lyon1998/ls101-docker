#include"Main.h"

void flash_erase_page(INT32U addr)  //erase a flash page, FLASH_PAGE_SIZE
{
	////////////////   入参检查END    ////////////////
	//FLASH_PEctrl_REG = 2;  // erase time, 2.5ms
	FLASH_PET_REG = 0x10;    // erase time, 2.5ms
	FLASH_CMD_REG = FLASH_ERASE_CMD | (addr & FLASH_ADDR_MASK);   //FLASH_ADDR_MASK确保每次擦除128bit，确保正确的Flash上线
}

void user_flash_erase(INT32U addr)
{
	INT32U i=0;

	INT32U start_addr = addr | LS1D_FLASH_ADDR;                //LS1D_FLASH_ADDR片上Flash的起始地址
	INT32U page = (0xbf01ffff - start_addr)/FLASH_PAGE_SIZE;   //0xbf01ffff片上Flash的上线，计算多少页
	
	for(i=0; i<page; i++)
	{
		flash_erase_page(start_addr+i*FLASH_PAGE_SIZE);
		delay_us(10);
	}
}

int ls1d_flash_read_page(INT32U addr, INT8U *data, INT32U num)
{
	int i = 0; 
	INT8U *ptr;

	if (addr + num > LS1D_FLASH_TOTAL)    //LS1D_FLASH_TOTAL:flash大小
		return -1;

	addr |= LS1D_FLASH_ADDR;
	ptr = (INT8U *)addr;

	for(i = 0; i < num; i++) 
	{
		*data++ = *ptr++;
	}

	return num;
}

INT32U ls1d_flash_write(INT32U addr, INT8U *data, INT32U num)
{
	INT32U temp_flash_buf;
	INT8U *temp_data = data;
	
	addr |= LS1D_FLASH_ADDR;
	FLASH_CMD_REG = FLASH_PAGE_LATCH_CLEAR;     //清 page_latch
	INT32U flash_block_mask = FLASH_PAGE_SIZE - 1;
	INT32U *page_data = (INT32U *)(addr & ~flash_block_mask) ;
	INT32U *old_data = (INT32U *)(addr & ~flash_block_mask) ;
	INT32U i, j, k, offset;

	j = 0;
	INT32U count = num;
	INT32U num_left = 0;
	INT32U num_right = 0;
	INT32U addr_left_align = 0xffffffff;
	INT32U addr_right_align = 0xffffffff;
	INT32U word_mask = WORD_WIDTH - 1;
	num = 0;

	num_left = addr & word_mask;
	if (num_left) 
	{
		num_left = WORD_WIDTH - num_left;
		num++;
		addr_left_align = (addr & flash_block_mask) >> 2;
	}
	j = count - num_left;
	
	num_right = (j & word_mask);
	if (num_right) 
	{
		num++;
		addr_right_align = addr + count - num_right;
		addr_right_align = (addr_right_align & flash_block_mask) >> 2;
	}
	j = count - num_left - num_right;
	j = j >> 2;
	num += j;

	offset = ((addr & flash_block_mask)>>2);

	for(j=0, i=0; i<(FLASH_PAGE_SIZE/4); i++) 
	{
		if( (offset <= i)  && (j < num) ) 
		{
			if (i == addr_left_align) 
			{
				int m = 0;
				temp_flash_buf = old_data[i];
				k = 1 << ((WORD_WIDTH - num_left) * 8);
				temp_flash_buf &= (k - 1);
				for (m = 0; m < num_left; m++) 
				{
					temp_flash_buf |= temp_data[m] << (WORD_WIDTH - num_left + m)*8;
				}
				temp_data = temp_data + num_left;	//address change to be logic 0 again.
				page_data[i] = temp_flash_buf;
				num--;                           	//because j don't add 1, num has to sub 1.
			}
			else if (i == addr_right_align)
			{
				temp_flash_buf = old_data[i];
				k = 1 << (num_right * 8);
				temp_flash_buf &= ~(k - 1);
				for (k = 0; k < num_right; k++) 
				{
					temp_flash_buf |= temp_data[j*4+k] << k*8;
				}
				j++;
				page_data[i] = temp_flash_buf;
			} 
			else
			{
				temp_flash_buf = 0;
				for (k = 0; k < WORD_WIDTH; k++) 
				{
					temp_flash_buf |= temp_data[j*4+k] << k*8;
				}
				j++;
				page_data[i] = temp_flash_buf;
			}
		} 
		else
			page_data[i] = old_data[i];
	}
	flash_erase_page(addr);  //erase a flash page, FLASH_PAGE_SIZE

	FLASH_PET_REG = 0x10;      // erase time, 2.5ms
	FLASH_CMD_REG = FLASH_WRITE_CMD | (addr & FLASH_ADDR_MASK) ;

	return TRUE;
}

void write_test(void)
{
    INT8U temp[128]={0};
    INT32U USER_BEGIN = (122 * 1024);
    int i;
    for(i=0;i<128;i++)
		temp[i] = i;
    ls1d_flash_write(USER_BEGIN, temp, FLASH_PAGE_SIZE);
}

void read_test(void)
{
    INT8U temp[128]={0};
    INT32U USER_BEGIN = (122 * 1024);
    int i;
    ls1d_flash_read_page(USER_BEGIN,temp,FLASH_PAGE_SIZE);
	 for(i=0;i<128;i++)
	     printf("%d \n",temp[i]);
}


