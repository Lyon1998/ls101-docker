#ifndef _RTC_H_
#define _RTC_H_

struct rtc_time {
     INT8U sec;
     INT8U min;
     INT8U hour;
     INT8U day;
     INT8U week;
     INT8U mon;
     INT8U year;
};

void rtc_write(struct rtc_time *Wtime);
void rtc_read(struct rtc_time *Rtime);


#endif 
