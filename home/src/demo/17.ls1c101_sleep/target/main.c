#define GLOBAL

#include"Main.h"

int main(void)
{
	PMU_WdtCfg = 0x80007fff;          //看门够设置最大时间
	uart1_init(); 
	os_clock_init();                  //使用外部8M时钟
	EnableInt();                      //使能全局中断
	
	if(spPMU_ChipCtrl->soft_flag == 0)                     //通过软件标志判断是否为第一次上电
    {
		printf("-------power supply wakeup------\n");      //第一次上电唤醒时,执行这里函数,常用放置一些系统只需要初始一次的函数或变量
		delay_ms(700);
		os_clock_ext32k();                                 //配置外部32k时钟
		spPMU_ChipCtrl->soft_flag = 0xa;                   //设置软件标志
    }
    else
    {
		printf("-------sleep wake up------\n");            //休眠唤醒则执行这里的函数
	}
	
	
	while(1)
	{
		sys_leep();
		printf("-------no wake-----\n");
	}

	return 0;
}


