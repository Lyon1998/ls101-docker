#include"Main.h"

/******************************************************************************
 ** \简  述  通过SPI接口读取1311的寄存器
 **
 ** \参  数  uint8_t regAdd: 寄存器地址
 ** \返回值  uint8_t 寄存器内容
 ******************************************************************************/
INT8U sky1311ReadReg(INT8U regAdd)
{	
    INT8U ucResult=0;
	SPI_SPSR = 0xc0;                       							                                    
	SPI_SFC_SOFTCS = 0xdf;                     // CSn1片选拉低
	
	SPI_TxFIFO = (regAdd & 0x3F) | 0x40;       
	while(SPI_SPSR & 0x01);                    //判断读寄存器是否为空，1表示为空
	ucResult = SPI_RxFIFO;

	SPI_TxFIFO = 0xFF;
	while(SPI_SPSR & 0x01);
	ucResult = SPI_RxFIFO;

	SPI_SFC_SOFTCS = 0xff;                     // CSn1片选拉高
	return ucResult;
}

/******************************************************************************
 ** \简  述  通过SPI接口向1311写一个字节的命令
 **
 ** \参  数  命令字
 ** \返回值  none
 ******************************************************************************/
void sky1311WriteCmd(INT8U cmd)
{
	INT8U ucResult=0;
	SPI_SPSR = 0xc0;
	SPI_SFC_SOFTCS = 0xdf;
	
	SPI_TxFIFO = (cmd&0x1f)|0x80;
	while(SPI_SPSR & 0x01);
	ucResult = SPI_RxFIFO;

	while(!(SPI_SPSR & 0x04));
	
	SPI_SFC_SOFTCS = 0xff;
}

/******************************************************************************
 ** \简  述  通过SPI接口向1311的寄存器写一个字节数据
 **
 ** \参  数  uint8_t regAdd: 寄存器地址， uint8_t data: 要写入的数据
 ** \返回值  none
 ******************************************************************************/
void sky1311WriteReg(INT8U regAdd, INT8U data)
{
	INT8U ucResult=0;
	SPI_SPSR = 0xc0;
	SPI_SFC_SOFTCS = 0xdf;
	
	SPI_TxFIFO = (regAdd & 0x3F);
	while(SPI_SPSR & 0x01);
	ucResult = SPI_RxFIFO;
	
	SPI_TxFIFO = data;
	while(SPI_SPSR & 0x01);
	ucResult = SPI_RxFIFO;
	SPI_SFC_SOFTCS = 0xff;	
}

/******************************************************************************
 ** \简  述  通过SPI接口向1311的FIFO写指定数目的数据
 **
 ** \参  数  uint8_t* 数据内容头地址， uint8_t count: 要写入的数据数量
 ** \返回值  none
 ******************************************************************************/
void sky1311WriteFifo(INT8U *data, INT8U count)
{
	INT8U i;
	for(i=0;i<count;i++)
	{
		sky1311WriteReg(ADDR_FIFO,data[i]);
	}
}
/******************************************************************************
 ** \简  述  通过SPI接口向1311的FIFO读取指定数目的内容
 **
 ** \参  数  uint8_t* data 保存读取内容的缓冲区首地址， uint8_t count 读取的字节数
 ** \返回值  none
 ******************************************************************************/
void sky1311ReadFifo(INT8U *data, INT8U count)
{
	INT8U i;
	for(i=0;i<count;i++)
	{
		data[i] = sky1311ReadReg(ADDR_FIFO);
	}
}

/******************************************************************************
 ** \简  述  设置寄存器掩码位
 **
 ** \参  数  uint8_t regAddr 寄存器地址， uint8_t mask 要设置的"掩码"字节
 ** \返回值  none
 ******************************************************************************/
void SetBitMask(INT8U regAddr, INT8U mask)
{
    INT8U tmp;
    tmp = sky1311ReadReg(regAddr);
    sky1311WriteReg(regAddr, tmp|mask);
}

/******************************************************************************
 ** \简  述  清除寄存器掩码位
 **
 ** \参  数  uint8_t regAddr 寄存器地址， uint8_t mask 要清除的"掩码"字节
 ** \返回值  none
 ******************************************************************************/
void ClearBitMask(INT8U regAddr, INT8U mask)
{
    INT8U tmp;
    tmp = sky1311ReadReg(regAddr);
    sky1311WriteReg(regAddr, tmp & ~mask);
}

/******************************************************************************
 ** \简  述  清除所有中断标记
 **
 ** \参  数  none
 ** \返回值  none
 ******************************************************************************/
void irqClearAll(void)
{
    sky1311WriteReg(ADDR_IRQ_STA, 0x7F);
}

/******************************************************************************
 ** \简  述  清除指定的中断标记
 **
 ** \参  数  uint8_t irq
 ** \返回值  none
 ******************************************************************************/
void irqClear(INT8U irq)
{
    sky1311WriteReg(ADDR_IRQ_STA,irq);
}

/******************************************************************************
 ** \简  述  数据发射函数，将数据写到FIFO中并通过射频接口发送给PICC
 **
 ** \参  数   uint8_t txType,        // 发射类型（TYPE_A | TYPE_B）
 **           uint8_t *txBuff,      // 数据内容
 **           uint16_t txSize       // 数据大小 要求不大于255
 ** \返回值    none
 ******************************************************************************/
void sky1311_fifo_tx(INT8U txType, INT8U *txBuff, INT16U txSize)
{
	INT8U  irq_sta;   // 中断请求状态代码
    volatile INT16U delayCount;
    sky1311WriteCmd(CMD_IDLE);              // reset state machine to Idle mode
    sky1311WriteCmd(CMD_CLR_FF);            // clear FIFO
    irqClearAll();                          // clear all IRQ state

    //sky1311WriteReg(ADDR_FIFO_CTRL,8);      // set water-level of FIFO
    /* write numbers */
    if(txType == TYPE_A){
        sky1311WriteReg(ADDR_TX_BYTE_NUM, txSize & 0x00ff);
    }else if(txType == TYPE_B){
        sky1311WriteReg(ADDR_TX_B_BYTE_NUM, txSize & 0x00ff);
    }
    /* when TX length<=FIFO's depth, write all data to FIFO */
    if( txSize <= 64){
		//printf("txLen shao 64\r\n");
        sky1311WriteFifo(txBuff, txSize);
        sky1311WriteCmd(CMD_TX_RX);             // transceive & into receive mode
    }
    
    /* wait TX finished */
    while(1)
	{
        delayCount = 0xFFF;
        while(!GPIO_ReadInputDataBit() && --delayCount);   // waiting for TX STOP IRQ
    
		if(!GPIO_ReadInputDataBit())
            return;
        
		irq_sta = sky1311ReadReg(ADDR_IRQ_STA);
        if(irq_sta & IRQ_TX)
		{
            irqClear(IRQ_TX);
            return;
        }
        else
		{
            irqClearAll();
        }
    }
}

/******************************************************************************
 ** \简  述  数据接收函数，将FIFO中数据读出来
 **
 ** \参  数   uint8_t txType,        // 发射类型（TYPE_A | TYPE_B）
              uint8_t rateType,      // 是否多倍速
 **           uint8_t *rxBuff,      // 数据内容
 **           uint16_t rxSize       // 数据大小
 ** \返回值  Ok,正确接收到数据 ; 其它，产生错误
 ******************************************************************************/
sta_result_t sky1311_fifo_rx(INT8U rxType, INT8U rateType, INT8U *rxBuff, INT16U *rxSize)
{
    INT8U rx_buf_cnt=0;
    INT8U  byte_num_H = 0;
    INT8U  byte_num_L = 0;
    INT8U  temp_len = 0;
    INT8U  bit_n=0;
	INT8U  err_sta;   // 错误状态代码
	INT8U  irq_sta;   // 中断请求状态代码
    
     volatile INT16U delayCount;

    while(1)
	{
        delayCount=0xFFF;                        // delay
        while(!GPIO_ReadInputDataBit() && --delayCount);
        if(delayCount == 0)
        {
            return NoResponse;
        }
        irq_sta = sky1311ReadReg(ADDR_IRQ_STA);
        err_sta = sky1311ReadReg(ADDR_ERR_STA);
        if(irq_sta & IRQ_TOUT) // tiemout
		{             
            sky1311WriteCmd(CMD_IDLE);
            irqClearAll();
            return NoResponse;
        }
        else if(irq_sta & IRQ_HIGH) // FIFO High
		{        
            sky1311ReadFifo(&rxBuff[rx_buf_cnt], 56);   // load next 56 bytes into FIFO
            rx_buf_cnt += 56;
            //irq_sta = sky1311ReadReg(ADDR_IRQ_STA);
            irqClear(IRQ_HIGH);//irqClearAll();
        }
        else if(irq_sta & IRQ_RX) // Received
		{       
            if( ((sky1311ReadReg(ADDR_FIFO_LEN))<2) || (err_sta & 0xC0) )
			{
                sky1311WriteCmd(CMD_CLR_FF);           // noise occur, restart the rx
                sky1311WriteCmd(CMD_RX);
                irqClearAll();
                return Error;
            }
            else
			{
                irqClearAll();
                if(err_sta & 0xc0)
                    return Error;
                temp_len = sky1311ReadReg(ADDR_FIFO_LEN);               // get FIFO length
                sky1311ReadFifo(&rxBuff[rx_buf_cnt], temp_len);        // get data ,FIFO-->rx_buf
                rx_buf_cnt += temp_len;

                if(rxType == TYPE_A)
				{
                    if(rateType)
					{
                     //   byte_num_H = sky1311ReadReg(ADDR_RATE_RX_BIT);
                     //   byte_num_L = sky1311ReadReg(ADDR_RATE_RX_BYTE);
                    }else
					{
                        byte_num_H = sky1311ReadReg(ADDR_RX_NUM_H);
                        byte_num_L = sky1311ReadReg(ADDR_RX_NUM_L);
                        bit_n = ((sky1311ReadReg(ADDR_RX_NUM_H)) & 0xf0)>>4;
                        if(bit_n) 
							sky1311ReadFifo(&rxBuff[rx_buf_cnt], 1);
                    }
                    *rxSize  = ((byte_num_H & 0x01) << 8) | byte_num_L;

                    if(bit_n)
                    {
                        *rxSize = ((*rxSize) + 1);
                    }
                }
                else if(rxType == TYPE_B)
				{
                    byte_num_H = sky1311ReadReg(ADDR_RX_B_CTRL);
                    byte_num_L = sky1311ReadReg(ADDR_RX_B_BYTE_NUM);
                    *rxSize  = ( (byte_num_H & 0x80) << 1 ) | byte_num_L;
                }
                return Ok;
            }
        }
        else
		{
            irqClearAll();
            return NoResponse;
        }
    }
}

/******************************************************************************
 ** \简  述  扫描最优的RC频率，为低功耗询卡用，
 **          注意：需要在读卡器场内没有任何导电物质遮挡时扫描
 **
 ** \参  数  none
 ** \返回值  低4位表示幅值最大时的频率值，对应Analog1寄存器的低4位。
   		 bit4 表示是否扫描到最大值，1--扫描到，0--没有。
   		 bit5--7 没有使用，设置为0
 ******************************************************************************/
INT16U sky1311RCFreqCali(void)
{	
    INT8U currAdcVal=0;                                     // 当前AD值
    INT8U maxAdcVal=0;                                      // 最大AD值
    INT8U currRCFreqVal=0;                                  // 当前频率值
    INT8U MaxRCFreqVal = 0;                                 // 最大的频率值
    sky1311Enable();
    delay_ms(5);
    sky1311WriteReg(ADDR_ANA_CFG1, 0x1C);                   // OSC Enable
	sky1311WriteReg(ADDR_ANA_CFG2, 0xA0);
   // sky1311WriteReg(ADDR_IRQ_EN, 0x00); 
	for(currRCFreqVal=0;currRCFreqVal<16;currRCFreqVal++)   // from 0000 to 1111 scan
    {        
        sky1311WriteReg(ADDR_ANA_CFG0, (currRCFreqVal << 4) | PA_2P0V);
        sky1311WriteReg(ADDR_ANA_CFG4, 0x00);
        delay_us(10);
        sky1311WriteReg(ADDR_ANA_CFG4, 0x80);        // AD转换 Trigger
        delay_us(50);								 // 延时约20微秒等待AD转换结束
        currAdcVal = sky1311ReadReg(ADDR_ANA_CFG5);  // read ADC value from analog5 register
        printf("---------currAdcVal = %x\r\n",currAdcVal);
        sky1311WriteReg(ADDR_ANA_CFG4, 0x00);        // Disable B6: RSSI_EN B5: TX_EN B4: ADC_EN
        if(currAdcVal > maxAdcVal){			         // 当前频率的AD值若大于最大值
            maxAdcVal = currAdcVal; 			     // 用当前值取代最大值
            MaxRCFreqVal = currRCFreqVal;   	     // 记下当前频率值
        }
        delay_us(100);
	}
	
	sky1311Disable();

    if(maxAdcVal>MINADVAL)
    {
        printf("RC frequency calibate, RC Param[%d] = %x\r\n",MaxRCFreqVal,maxAdcVal);
        return ((0x10 | MaxRCFreqVal) << 8 | maxAdcVal);		// 返回成功标记和频率值
    }
    else
    {
        printf("RC frequency calibrate: Error!\r\n");
        return 0;
    }
}


/******************************************************************************
 ** \简  述  检查是否有卡进入或者离开场(低功耗询卡)
 **
 ** \参  数  uint8_t RC频率设定值
 ** \返回值  没有动作，0  (设置低功耗询卡标记)
 **          有卡进入，1  (设置读卡标记)
 **          有卡离开，2  (设置重新校准查询卡频率标记)
 ******************************************************************************/
void checkCardInit(INT16U rc_val)
{
	INT8U MaxRCFreqVal, MaxADCVal;

    MaxRCFreqVal = (INT8U)((rc_val >> 4) & 0xF0); // RC频率校准值

    MaxADCVal = (INT8U)((rc_val & 0xFF));         // 对应的最大AD值
    /* RC校准时获得的AD值可能比实际自动检卡时的AD值偏大，
    如果这样可以将ＭaxADCVal值减去一定的值，减去的值根据实际情况确定 */
  //  MaxADCVal -= 16;
	sky1311WriteReg(ADDR_ANA_CFG1, 0x00);
    sky1311Enable();
    delay_ms(5);
    sky1311WriteReg(ADDR_ANA_CFG0, MaxRCFreqVal | PA_2P0V);
    
    sky1311WriteReg(ADDR_ANA_CFG2, 0x10);
    sky1311WriteReg(ADDR_ANA_CFG4, 0x00);
    //sky1311WriteReg(ADDR_ANA_CFG6, WKU_EN | WKU_OR | WKU_300MS | RSSI_2AVG);
    sky1311WriteReg(ADDR_ANA_CFG6, WKU_EN | WKU_AND | WKU_300MS | RSSI_2AVG);
    sky1311WriteReg(ADDR_ANA_CFG7, ADC_SAMPLE_5US | TX_SETTLE_0US | RSSI_DELTA);
    sky1311WriteReg(ADDR_ANA_CFG8, MaxADCVal-RSSI_ABS);
   
    irqClearAll();
    sky1311WriteReg(ADDR_ANA_CFG1, 0x1C);
    sky1311Disable();
}

/***************************************************************************
 * 不检卡
 * *************************************************************************/
#if 0
void checkCardDeinit(void)
{
    sky1311Enable();
    delay_ms(5);
    sky1311WriteReg(ADDR_ANA_CFG6, 0x22);
    sky1311WriteReg(ADDR_ANA_CFG7, 0x3F);
    sky1311WriteReg(ADDR_ANA_CFG8, 0xFF);
    sky1311Disable();
}
#endif

/******************************************************************************
 ** \简  述  模拟参数初始化，配置模拟参数寄存器
 **
 ** \参  数  none
 ** \返回值  none
 ******************************************************************************/
void analogInit(void)
{
    sky1311WriteReg(ADDR_ANA_CFG0, PA_2P0V);   // 7-4: RC OSC Freq
                                             // 3-2: PA Driver
                                             //   1: ixtal
                                             //   0: External LDO
    sky1311WriteReg(ADDR_ANA_CFG2, ANA2_A);
    //sky1311WriteReg(ADDR_ANA_CFG1, 0xFC);   //  7: txen
                                              //  6: rxen
                                              //  5-4: clk_sel
                                              //     00(osc off, xtal off)
                                              //     01(osc on, xtal off)
                                              //     10(osc off, xtal 13.56)
                                              //     11(osc off, xtal 27.12)
    sky1311WriteReg(ADDR_ANA_CFG1, 0x00);
    //DelayMS(1);
    delay_us(100);
    sky1311WriteReg(ADDR_ANA_CFG1, 0xFC);   //sky1311WriteReg(ADDR_ANA_CFG1, 0xF4);
}

/******************************************************************************
 ** \简  述  初始化SKY1311S
 **
 ** \参  数  none
 ** \返回值  none
 ******************************************************************************/
void sky1311Init(void)
{
	sky1311WriteCmd(CMD_SW_RST);                // reset status
    analogInit();
    delay_ms(5);  //Crystal Stable time    
    sky1311WriteReg(ADDR_TIME_OUT2, 0x8F);       // time_out timer stop condition = beginning of RX SOF
    sky1311WriteReg(ADDR_TIME_OUT1, 0xFF);
    sky1311WriteReg(ADDR_TIME_OUT0, 0xFF);
    sky1311WriteReg(ADDR_RX_PUL_DETA, 0x34);     // 高4位识别曼彻斯特码脉宽，越大容错能力越强
    sky1311WriteReg(ADDR_RX_PRE_PROC, 0x00);
    sky1311WriteReg(ADDR_RX_START_BIT_NUM,0);
    sky1311WriteReg(ADDR_MOD_SRC,0x02);
    sky1311WriteReg(ADDR_IRQ_EN,IRQ_TOUT_EN|IRQ_TX_EN|IRQ_RX_EN|IRQ_HIGH_EN|IRQ_LOW_EN);    // enable all IRQ
    sky1311WriteReg(ADDR_FIFO_CTRL,8);            // set water-level of FIFO
}

/******************************************************************************
 ** \简  述  SKY1311S复位，并处于disable状态
 **
 ** \参  数  none
 ** \返回值  none
 ******************************************************************************/
void sky1311Reset(void)
{
	sky1311WriteReg(ADDR_ANA_CFG1, 0x04);   // close TX, RX, OSC off
    sky1311WriteCmd(CMD_SW_RST);
    sky1311Disable();
}

/******************************************************************************
 ** \简  述  复位PICC，关闭场5ms,再打开
 **
 ** \参  数  none
 ** \返回值  none
 ******************************************************************************/
#if 0
void resetPicc(void)
{
    ClearBitMask(ADDR_ANA_CFG1,TX_EN);
    delay_ms(5);
    SetBitMask(ADDR_ANA_CFG1,TX_EN);
    delay_ms(10);
}
/******************************************************************************
 ** \简  述  检查是否有卡离开场配置
 **
 ** \参  数  uint16_t RC频率扫描返回值
 ** \返回值  无
 ******************************************************************************/
void checkCardRemoveConfig(INT16U rc_val)
{
	INT8U MaxRCFreqVal;
    MaxRCFreqVal = (INT8U)((rc_val >> 4) & 0xF0);
    sky1311Enable();
    delay_ms(5);
    sky1311WriteReg(ADDR_ANA_CFG0, MaxRCFreqVal | PA_2P0V);
    sky1311WriteReg(ADDR_ANA_CFG1, 0x1C);
    sky1311WriteReg(ADDR_ANA_CFG2, 0xA0);
    sky1311Disable();
}

/******************************************************************************
 ** \简  述  检查是否有卡离开场
 **
 ** \参  数  uint8_t RC频率扫描得到的最大AD值
 ** \返回值  true,卡已经离开； false， 卡没有离开
 ******************************************************************************/
char checkCardRemove(INT8U maxAdcVal)
{
	INT8U currAdcVal;       
    sky1311Enable();
    delay_ms(5);
    sky1311WriteReg(ADDR_ANA_CFG4, 0x00);
    delay_us(3);
    sky1311WriteReg(ADDR_ANA_CFG4, 0x80);
    delay_us(20);
    currAdcVal = sky1311ReadReg(ADDR_ANA_CFG5);
    sky1311WriteReg(ADDR_ANA_CFG4, 0x00);
    sky1311Disable();
    if(currAdcVal > (maxAdcVal - RSSI_ABS))
    {
        return true;
    }
    else
	{
        return false;
	}
}
#endif
/**********************************TypeA start************************************/
/******************************************************************************
 ** \简  述  选择type A 卡作为操作对象
 **
 ** \参  数  none
 ** \返回值  none
 ******************************************************************************/
void typeAOperate(void)
{
    sky1311WriteReg(ADDR_ANA_CFG2, ANA2_A);             // analogA select
    sky1311WriteReg(ADDR_FSM_STATE, TYPE_A_SEL);        // typeA select
    sky1311WriteReg(ADDR_TX_PUL_WID,0x26);              // set to default value
    sky1311WriteReg(ADDR_CRC_CTRL, CRC_A);              // crcA enable
    sky1311WriteReg(ADDR_M1_CTRL, 0x00);                // disable M1 operation
    sky1311WriteReg(ADDR_ANA_CFG3, 0x80);
}

/******************************************************************************
 ** \简  述  type A "Wake-Up" 操作,command = 52H
 **
 ** \参  数  none
 ** \返回值  sta_result_t 操作状态，Ok：成功，其它：失败
 ******************************************************************************/
sta_result_t piccWakeupA(INT8U *ATQA)
{
    sta_result_t sta;
    INT16U tmpSize;
    INT8U tmpBuf[1];

    tmpBuf[0] = WUPA;                                               // 0x52
    sky1311WriteReg(ADDR_TX_CTRL, TX_POLE_HIGH|TX_PARITY_ODD);      // TX odd parity, no CRC
    sky1311WriteReg(ADDR_RX_CTRL, RX_PARITY_EN|RX_PARITY_ODD);      // RX odd parity, no CRC
    sky1311WriteReg(ADDR_TX_BIT_NUM, 0x07);
    sky1311WriteReg(ADDR_TX_BYTE_NUM, 0x01);
    sky1311_fifo_tx(TYPE_A, tmpBuf, 1);
	sta = sky1311_fifo_rx(TYPE_A, RATE_OFF, ATQA, &tmpSize);
    return sta;
}

/******************************************************************************
 ** \简  述  Type A 防冲突数据发送函数，将数据写到FIFO中并通过射频接口发送给PICC
 **          发送的数据保存在全局变量tx_buf中，发送缓冲区
 **
 ** \参  数  uint8_t* txBuf           数据缓冲区
 **          uint8_t txLen          发射的数据长度（包括不完整的部分）,
 **          uint8_t lastBitNum     最后一个字节有效位数
 ** \返回值  Ok--成功发射，Timeout--超时, Error -- 其它错误
 ******************************************************************************/
sta_result_t bitCollisionTrans(INT8U* txBuf, INT8U txLen, INT8U lastBitNum)
{
	INT8U  irq_sta;   // 中断请求状态代码
    volatile INT16U delayCount;
    if(txLen>7)                             // The maximum length of for transmission from PCD to PICC shall be 55 data bits
        return Error;
    sky1311WriteCmd(CMD_IDLE);              // reset state machine to Idle mode
    sky1311WriteCmd(CMD_CLR_FF);            // clear FIFO
    irqClearAll();                          // clear all IRQ state
    /* write numbers */
    sky1311WriteReg(ADDR_TX_BYTE_NUM, txLen);
    sky1311WriteReg(ADDR_TX_BIT_NUM,lastBitNum);
    sky1311WriteFifo(txBuf, txLen);         // write data to FIFO
    sky1311WriteCmd(CMD_TX_RX);             // transceive & into receive mode
    /* wait TX finished */
    while(1)
	{
        delayCount = 0xFFF;                // timeout count
        while(! GPIO_ReadInputDataBit() && delayCount--);   // waiting for TX STOP IRQ

		if(! GPIO_ReadInputDataBit())
            return Timeout;
        
		irq_sta = sky1311ReadReg(ADDR_IRQ_STA);
        if(irq_sta & IRQ_TOUT)
		{
            sky1311WriteCmd(CMD_IDLE);
            irqClearAll();
            return Timeout;
        }
        else if(irq_sta & IRQ_TX)
		{
            irqClear(IRQ_TX);
            break;
        }
        else
		{
            irqClearAll();
        }
    }
   
    // 等待接收数据完成
    delayCount = 0xFFF;                            // timeout count
    while(! GPIO_ReadInputDataBit() && delayCount--);               // waiting for TX STOP IRQ
    
	if(! GPIO_ReadInputDataBit())
        return Timeout;
    irq_sta = sky1311ReadReg(ADDR_IRQ_STA);
    if(irq_sta & IRQ_TOUT)
	{
        sky1311WriteCmd(CMD_IDLE);
        irqClearAll();
        return Timeout;
    }else if(!(irq_sta & IRQ_RX))
	{
        irqClearAll();
        return Error;
    }
    irqClearAll();

    return Ok;
}


/******************************************************************************
 ** \简  述  type A卡防冲突循环
 **
 ** \参  数  SEL,  (93h, 95h, 97h)
 **          uint8_t randBit 位冲突时选择的数，0或1;
 **          uint8_t* uid 获得的UID保存区域的首地址
 ** \返回值  return Ok on success, Error on error
 ******************************************************************************/
sta_result_t piccAntiA(INT8U SEL, INT8U rand_bit, INT8U *uid)
{
    INT8U NVB= 0x20;
    INT8U currValidBytes=0, currValidBits=0;
    INT8U recBytes, recBits;
    INT8U recUID[5]= {0};      // 接收到的UID数
    INT8U hasCollision = 0;
	INT8U tmpSize = 0;
	INT8U tmpBuf[12] = {0};
	INT8U g_rx_buff[12] = {0};
    INT8U i;

    /* config registor of sky1311s */
    sky1311WriteReg(ADDR_RX_NUM_H, COLL_EN);        // anti-collision on
    sky1311WriteReg(ADDR_TX_CTRL, TX_POLE_HIGH|TX_PARITY_ODD);

    /* STEP 1: send SEL + 20H to PICC */
    tmpBuf[0] = SEL;
    tmpBuf[1] = NVB;
    if(Ok != bitCollisionTrans(tmpBuf, 2 , 8))
	{
        sky1311WriteReg(ADDR_RX_NUM_H, 0x00);               // disable anti-collision
        return Error;
    }
    delay_us(5);
    do{
        /* 读出PICC返回的数据字节数和不完整的位数，将数据保存到缓冲区 */
        recBytes = sky1311ReadReg(ADDR_RX_NUM_L);
        recBits = ((sky1311ReadReg(ADDR_RX_NUM_H) & 0xf0) >> 4);
        tmpSize = sky1311ReadReg(ADDR_FIFO_LEN);
        sky1311ReadFifo(g_rx_buff, tmpSize);
        if((sky1311ReadReg(ADDR_RX_NUM_H) & RX_FORBID) == 0)   // no collision happens
            hasCollision = 0;
        else
            hasCollision = 1;

        /* 将收到的UID数据和之前的数据拼接合并 */
        if(currValidBits == 0)         // 当前UID全部是完整的字节，按字节拼接
		{
            for( i=0;i<tmpSize;i++)
			{
                recUID[currValidBytes++] = g_rx_buff[i];
            }
            currValidBits = recBits;
        }
        else                           // 当前UID最后一个字节不完整，第一个字节需要按位拼接
		{
             if(recBytes == 0)              // 仅收到一个字节新数据（可能也不完整）
			 {
                recUID[currValidBytes-1] += g_rx_buff[0]<<currValidBits;    // 按位拼接第一个字节
                currValidBits += recBits;
                if(currValidBits==8)
				{
                    currValidBytes++;
                    currValidBits = 0;
                }
            }
            else                           // 收到多个字节数据
			{
                recUID[currValidBytes-1] += g_rx_buff[0];//<<currValidBits;   // 按位拼接第一个字节
                for( i=1;i<tmpSize;i++)           // 复制剩余的字节
				{
                    recUID[currValidBytes++] = g_rx_buff[i];
                }
                currValidBits = recBits;
            }
        }
        
        if(hasCollision)
		{
            delay_us(5);
            /* 将UID数据加上rand_bit */
            if(currValidBits==0)
			{
                recUID[currValidBytes] = rand_bit;
                currValidBytes++;
                currValidBits = 1;
            }
            else if(currValidBits == 7)
			{
                recUID[currValidBytes-1] += rand_bit<<7;
                currValidBytes++;
                currValidBits = 0;
            }
            else       // currValidBits= 1 ... 6
			{
                recUID[currValidBytes-1] += rand_bit<<currValidBits;
                currValidBits++;
            }
            /* 计算新的NVB，并将当前UID+rand_bit传送给PICC */
            NVB = (0x20+((currValidBytes-1)<<4)) |  currValidBits;
            tmpBuf[0] = SEL;
            tmpBuf[1] = NVB;
            for( i=0;i<currValidBytes;i++)
			{
                tmpBuf[2+i] = recUID[i];
            }
            if(Ok != bitCollisionTrans(tmpBuf, currValidBytes+2 , currValidBits))
			{
                sky1311WriteReg(ADDR_RX_NUM_H, 0x00);// disable anti-collision
                return Error;
            }
        }

    }while(hasCollision);

    sky1311WriteReg(ADDR_RX_NUM_H, 0x00); // disable anti-collision
	memcpy(uid,recUID,5);
    return Ok;
}

/************************** Type A PICC test ****************************/
sta_result_t TypeA_test(void)
{
    sta_result_t sta;
    INT8U UID[10] = {0};
    INT8U tmpBuf[10] = {0};
	
	sky1311Enable(); 	                        // chip enable PD2
	sky1311Init();			                    // init sky1311s
	//resetPicc();                              // 复位卡片()
	
    sky1311WriteReg(ADDR_ANA_CFG3, 0x81);
    typeAOperate();

    //sta = piccRequestA(tmpBuf);                   // send REQA command to card
    sta = piccWakeupA(tmpBuf);                      // send WUPA command to card


{
    if(Ok == sta)
    {
		
		sta = piccAntiA(SEL1, 0x01, UID);   // 防碰撞循环，测试获得一张卡的UID，参数0x0表示遇到冲突时选择1
	
		if(Ok == sta)
		{
#if 1
			printf("UID:");
			int i;
			for(i=0;i<5;i++)
			{
				printf("%2x ",UID[i]);
			}
			printf("\r\n");
#endif 	
			//memcpy(CARD_ID,UID,5);
			INT8U cmp_id[4] = {0};
			if(memcmp(&UID[1],cmp_id,4))
			{
				memset(CARD_ID,0x00,8);
				memcpy(CARD_ID,UID,5);
			}
			else
			{
                sta = Error;
			}
		}
    }
}

	sky1311Reset();
	return sta;
}
/**************************************Type_A end***************************************/

/**************************************Type_B start*************************************/
/******************************************************************************
 ** \简  述  选择type B 卡作为操作对象
 **
 ** \参  数  none
 ** \返回值  none
 ******************************************************************************/
void typeBOperate(void)
{
    sky1311WriteReg(ADDR_ANA_CFG2, ANA2_B);            // analogB select
    sky1311WriteReg(ADDR_FSM_STATE, TYPE_B_SEL);       // typeB select
    sky1311WriteReg(ADDR_CRC_CTRL, CRC_B);             // crcB enable
}

/******************************************************************************
 ** \简  述  type B 请求命令REQB, 返回值是 ATQB。
 **
 **
 ** \参  数  uint8_t ucReqCode:请求代码 ISO14443_3B_REQIDL 0x00 -- 空闲，就绪的卡
 **								  ISO14443_3B_REQALL 0x08 -- 空闲，就绪，退出的卡
 **			 uint8_t ucAFI ：应用标识符，0x00：全选
 **			 uint8_t N：时隙总数,取值范围0--4。
 **          nAQTB_t *pATQB 请求应答返回数据指针，12字节
 ** \返回值  操作状态，Ok：成功， 其它值：失败
 ******************************************************************************/
sta_result_t piccRequestB(INT8U ucReqCode, INT8U ucAFI, INT8U N, INT8U *PUPI)
{
    sta_result_t sta;
    INT8U tmpBuf[16];
    INT16U tmpSize;

    tmpBuf[0] = APF_CODE;         // APf = 0x05
    tmpBuf[1] = ucAFI;            // AFI, 00,选择所有PICC
    tmpBuf[2] = (ucReqCode & 0x08)|(N & 0x07);
    //sta = data_tx_rx(3, TYPE_B, RATE_OFF);
    sky1311_fifo_tx(TYPE_B, tmpBuf, 3);
    sta = sky1311_fifo_rx(TYPE_B, false, tmpBuf, &tmpSize);
    if(sta == Ok)
	{
        PUPI[0] = tmpBuf[1];
        PUPI[1] = tmpBuf[2];
        PUPI[2] = tmpBuf[3];
        PUPI[3] = tmpBuf[4];             // 4 Bytes PUPI
    }
    return sta;
}

/******************************************************************************
 ** \简  述  选择type B PICC
 **
 ** \参  数     uint8_t *pPUPI					    // 4字节PICC标识符
//				uint8_t ucDSI_DRI					// PCD<-->PICC 速率选择
//				uint8_t MAX_FSDI				    // PCD最大接收缓冲区大小
//				uint8_t ucCID						// 0 - 14,若不支持CID，则设置为0000
//				uint8_t ucProType					// 支持的协议，由请求回应中的ProtocolType指定
//				uint8_t *pHigherLayerINF			// 高层命令信息
// 出口参数:    uint8_t *pAATTRIB					// ATTRIB命令回应
//				uint8_t *pRLen					    // ATTRIB命令回应的字节数
 ** \返回值  状态 Ok成功， 其它失败
 ******************************************************************************/
sta_result_t piccAttrib(INT8U *pPUPI, INT8U ucDSI_DRI,INT8U MAX_FSDI,
				 INT8U ucCID, INT8U ucProType,INT8U *pAATTRIB, INT8U *pRLen)
{
    sta_result_t sta;
    INT8U tmpBuf[32];
    INT16U tmpSize;
    tmpBuf[0] = APC_CODE;     // 0x1D
    tmpBuf[1] = pPUPI[0];
    tmpBuf[2] = pPUPI[1];
    tmpBuf[3] = pPUPI[2];
    tmpBuf[4] = pPUPI[3];                             // 4 Bytes PUPI
    tmpBuf[5] = 0x00;                               // param 1
    tmpBuf[6] = ((ucDSI_DRI << 4) | (MAX_FSDI & 0x0F)) & 0xFF;// param 2, rate and length
    tmpBuf[7] =  ucProType & 0x0f;

    tmpBuf[8] = ucCID & 0x0f;
    sky1311_fifo_tx(TYPE_B, tmpBuf, 9);
    sta = sky1311_fifo_rx(TYPE_B, false, tmpBuf, &tmpSize);

    if(sta == Ok)
	{
        *pRLen = tmpSize;
        memcpy(pAATTRIB, tmpBuf, tmpSize);
    }
    return sta;
}

/***************************************************************
    TYPE B卡操作示例
   读取TYPE B卡的UID等，并使用部分APDU做测试
   寻卡等操作时，显示操作结果
   有错误时显示错误信息
****************************************************************/
INT8U SmartTypeB_test(void)
{
    sta_result_t sta;

	INT8U  PUPI[4];					// 伪唯一PICC标识符
    INT8U  tmpBuf[32];
    INT16U tmpSize;
    INT8U  attSize;
	
	sky1311Enable(); 	                        // chip enable PD2
	sky1311Init();			                    // init sky1311s
	//resetPicc();                              // 复位卡片()

    typeBOperate();
    sta = piccRequestB(REQALL,0, 0, PUPI);
   
    if(Ok == sta)
	{

		sta = piccAttrib(PUPI, 0, 8, 0, 1, tmpBuf, &attSize);
	
		if(Ok == sta)
		{
			/* 读身份证ID */
			tmpBuf[0] = 0x00;
			tmpBuf[1] = 0x36;
		    tmpBuf[2] = 0x00;
		    tmpBuf[3] = 0x00;
		    tmpBuf[4] = 0x08;
		    sky1311_fifo_tx(TYPE_B, tmpBuf, 5);
		    sta = sky1311_fifo_rx(TYPE_B, RATE_OFF, tmpBuf, &tmpSize);
			if(Ok == sta)
			{
#if 1
		        printf("ID Card:");
		        int i;
		        for(i=0;i<tmpSize-3;i++)
		        {
					printf("%2x ",tmpBuf[i]);
				}
				printf("\r\n");
#endif
				//memcpy(CARD_ID,tmpBuf,8);
				if(tmpBuf[8] == 0x90)
				{
					memcpy(CARD_ID,tmpBuf,8);
				}
				else
				{
					sta = NoResponse;
				}

			}
		}
	}
	sky1311Reset();
    return sta;
}

void Check_Card1(void)
{
	gpio_pin_remap(GPIO_PIN_2,GPIO_FUNC_MAIN);
	gpio_write_pin(POWER_RFID,GPIO_LOW); 
	
	SPI_INIT(); 
	printf("--------123----------\n\r");
	while(1)
	{
		wdt_dog_feed();
		if(Ok != TypeA_test())
		{
			if(Ok!= SmartTypeB_test())
			{
				printf("--------Fill----------\n\r");
			}
		}
		delay_ms(200);
	 }
}


