#include"Main.h"

void i2c_init(void)
{
	gpio_pin_remap(GPIO_PIN_4,GPIO_FUNC_MAIN);
	gpio_pin_remap(GPIO_PIN_5,GPIO_FUNC_MAIN);
    I2C_CTR 	= 0x20;        //设置为主设备模式,访问分频寄存器
    I2C_PRERL	= 0x13;    
    I2C_PRERH	= 0x0;
    I2C_CTR 	= 0xa0;        //设置为主设备模式,正常访问
    I2C_CR		= 0x04;
    delay_ms(1);
}

void i2c_wait(void)
{
    while (I2C_SR & 0x2);      //等待空闲
}

void TM1605_write(INT32U reg_addr,INT32U data)
{
	i2c_init();
	i2c_wait();
	
	I2C_TXR = reg_addr & 0xff | 0;        //发送寄存器地址
    I2C_CR  = 0x90;
    i2c_wait();
    if(I2C_SR & 0x80) {
        printf("\n no ack \n");
        return;
    } 
    printf("----IIC I2C_SR=%x-----\n",I2C_SR & 0x8);
    
    
    I2C_TXR = data & 0xff;          //发送数据
    I2C_CR = 0x10;                
    i2c_wait();
    
    I2C_CR     = 0x40;
    i2c_wait();
    
    printf("\n i2c1 write slv %x with %x", reg_addr & 0xff, data & 0xff);
}

void led_public(void)
{
	gpio_write_pin(POWER_LED,GPIO_LOW);
	TM1605_write(0x48,(0*16+0x01));
	TM1605_write(0x68,0x00); 
	TM1605_write(0x6a,0x00); 
	TM1605_write(0x6c,0x00); 
	TM1605_write(0x6e,0x00);
	delay_ms(1);
}

void loop_led(void)
{
	int i;
	int time = 500;
	led_public();
	
	for(i=0;i<3;i++)
	{
		TM1605_write(0x68,0x01);                    	
		delay_ms(time);
		TM1605_write(0x68,0x02);                    	
		delay_ms(time);
		TM1605_write(0x68,0x04);                    	
		delay_ms(time);
		
		TM1605_write(0x68,0x00);   
		delay_ms(1);
		
		TM1605_write(0x6a,0x04);                    	
		delay_ms(time);
		TM1605_write(0x6a,0x00);   
		delay_ms(1);
		
		TM1605_write(0x6c,0x04);                    	
		delay_ms(time);
		TM1605_write(0x6c,0x00);   
		delay_ms(1);
		
		TM1605_write(0x6e,0x04);                    	
		delay_ms(time);
		TM1605_write(0x6e,0x00);   
		delay_ms(1);
		
		TM1605_write(0x6e,0x02);                    	
		delay_ms(time);
		TM1605_write(0x6e,0x00);   
		delay_ms(1);
		
		TM1605_write(0x6e,0x01);                    	
		delay_ms(time);
		TM1605_write(0x6e,0x00);   
		delay_ms(1);
		
		TM1605_write(0x6c,0x01);                    	
		delay_ms(time);
		TM1605_write(0x6c,0x00);   
		delay_ms(1);
		
		TM1605_write(0x6a,0x01);                    	
		delay_ms(time);
		led_public();
	}
}





