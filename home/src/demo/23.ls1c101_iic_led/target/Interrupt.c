#include"Main.h"

void SOFT_INT(void)
{
}

void TIMER_8M_INT(void)
{
}

void TIMER_WAKE_INT(void)
{
	printf("\n.............TIMER_WAKE_INT..............\n\r");
	PMU_CommandW = (1<<16);                                         // 清除中断标志
	//TODO:处理系统需要定时唤醒的时间,比如喂狗
	INT32U SleepEn = PMU_CmdSts & 0xffffffff;
		
	wdt_dog_feed();             //睡眠后，定时喂狗，否则会不断重启
		
	while(SleepEn&(1<<0))     //读出SleepEn位为1时，表示可休眠。
	{
		PMU_CommandW = 0x1;                   //开启睡眠
	}
}

void TOUCH(void)
{
	printf("\n.............TOUCH INT..............\r\n");
	
	PMU_CommandW = (1<<17);                            // 清除中断标志
    TS_STAT = 0x07;                                    //检测到按键按下动作；检测到按键抬起动作；检测到变化，进入到激活模式。
	
	key_read_value();
	
}

void UART2_INT(void)
{
	printf("\n.............UART2 INT..............\r\n"); 
	PMU_CommandW = (1<<18);
}

void BAT_FAIL(void)
{
}

void INTC(void)
{
	INT8U IntReg = INT_OUT;
	
	if(IntReg&TIMER_INT_OUT)
	{
		printf("------timer int------\n");
		TIMER_CFG = TIMER_CFG;    // 读出中断状态并写回,用于清除中断
		//TODO: 做相关的中断处理函数
		
		INT_CLR = TIMER_INT_CLR; 
	}
	
	if(IntReg&UART1_INT_OUT)    					//Uart1
	{
		INT8U uart_sr = Uart1_IIR;	;

		if(uart_sr&0x04)
        {
			while(Uart1_LSR&0x1)
			{
				/*uart_rec[uart_rec_len] = Uart1_RxData;
				uart_rec_len = (uart_rec_len+ 1) % REV_NUM;
				if(uart_rec_len>9)
				{
					printBuf(uart_rec,10);
					uart_rec_len = 0;
				}*/
			}
        }
       	INT_CLR = UART1_INT_CLR;
	}
	
	if(IntReg&UART0_INT_OUT)    //Uart0
	{
		INT_CLR = UART0_INT_CLR;
	}
	
	INT_CLR = 0xff;
	
}

/******* IO中断 *******/
void RING(void)
{

}

/******	查询中断源 ********/
void IRQ_Exception()
{
    INT32U bak = 0;
    INT32U irq_no = 0;
    asm volatile("mfc0  %0, $13":"=r" (bak));

    for(irq_no = 8; irq_no < 16; irq_no++)
    {
    	if((bak>>irq_no) & 0x1)
    	{
    		SYS_IrqHandle[irq_no-8]();//进入中断处理程序
    	}
    }
}

