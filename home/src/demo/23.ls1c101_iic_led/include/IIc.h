#ifndef _IIC_H_
#define _IIC_H_

#define I2C_START				        0x80
#define I2C_STOP				        0x40
#define I2C_READ				        0x20
#define I2C_WRITE				        0x10
#define I2C_WACK				        0x8
#define I2C_IACK				        0x1
#define I2C_RUN					        0x2
#define I2C_BUSY				        0x40
#define I2C_RACK				        0x80
#define SR_NOACK				        0x80
#define SR_BUSY					        0x40 
#define SR_TIP					        0x2
#define CR_WRITE				        0x10
#define CR_READ					        0x20
#define CR_STOP					        0x40
#define CR_START				        0x80
#define I2C_SINGLE				        0 
#define I2C_BLOCK				        1
#define I2C_SMB_BLOCK			        2

#define POWER_LED                       GPIO_PIN_36

void i2c_init(void);
void i2c_wait(void);
void TM1605_write(INT32U reg_addr,INT32U data);
void led_public(void);
void loop_led(void);


#endif


