#define GLOBAL

#include"Main.h"

/*
 * 使用外部8M时钟和外部32k时钟
 */

int main(void)
{
	PMU_WdtCfg = 0x80007fff;          //看门够设置最大时间
	os_clock_init();                  //使用外部8M时钟
	os_clock_ext32k();
	
	while(1)
	{
		if((PMU_CmdSts&(1<<29)) == 1)           //如果初始化时没有配置成功，则继续配置32时钟
		{
			PMU_ChipCtrl &= ~(1<<5);
			delay_us(30);
			PMU_ChipCtrl |= (1<<5); 
			delay_us(30);
		}
	}

	return 0;
}


