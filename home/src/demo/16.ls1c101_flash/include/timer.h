#ifndef _TIMER_H_
#define _TIMER_H_

void timer_int(void);
void rtc_timer_count(INT32U settime,INT32U systime);
INT32U get_current_timer;

#endif
