#ifndef _STR_OTHER_H_
#define _STR_OTHER_H_

#define NULL    ((void *)0)

int 	strlen(const char * p);
char 	* strcpy(char *dest, const char *src);
int 	strcmp(const char *s1, const char *s2);
char 	*strcat(char *dst, const char *src);
void 	itoa(char chWord[], int Num);
int 	atoi(char *pstr);
unsigned char my_strstr(const unsigned char *str, const unsigned char *sub_str,unsigned char num);
void 	*memset(void * s,int c, int count);
void 	*memcpy(void *s1, const void *s2, int n);
int 	memcmp(const void * cs,const void * ct,int count);
#endif
