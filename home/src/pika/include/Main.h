#ifndef MAIN_H_
#define MAIN_H_

typedef unsigned char       	INT8U;
typedef signed char				INT8S;
typedef unsigned short      	INT16U;
typedef signed short        	INT16S;
typedef unsigned int         	INT32U;//32位
typedef signed int           	INT32S;
typedef unsigned long       	INT64U;//64位
typedef signed long         	INT64S;
typedef float					FP32;
typedef float					FP64;

typedef unsigned char 		    BOOL;

#define NO_INIT volatile

#include"ls1c101.h"
#include"Common.h"
#include"Interrupt.h"



#ifdef  GLOBAL

///////////////////////////////////////////////////////////////////////////////
//////////////////////////////  以下定义数据表格  /////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void (* const SYS_IrqHandle[])(void) = {
		SOFT_INT,
        TIMER_8M_INT,
        TIMER_WAKE_INT,
        TOUCH,
        UART2_INT,
        BAT_FAIL,
        INTC,
        RING
};
#endif

#ifdef GLOBAL
#define EXT
#else
#define EXT  extern
#endif

EXT void	 (* const SYS_IrqHandle[])(void);

#endif


