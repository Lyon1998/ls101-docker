#include"Main.h"

void uart1_init(void)
{
	gpio_pin_remap(GPIO_PIN_8,GPIO_FUNC_MAIN);          //管脚复用为RX
	gpio_pin_remap(GPIO_PIN_9,GPIO_FUNC_MAIN);          //管脚复用为TX
	
    Uart1_LCR = CLCR_DLAB;                              //Baud rate set as 115200
    Uart1_DL_H = 0x0;
    Uart1_DL_L = 0x4;
    Uart1_DL_D = 0x57;
    Uart1_LCR = CLCR_8BITS; 
    Uart1_FCR = FIFO_TRIGGER_1 | FIFO_TX_RST | FIFO_RX_RST;
}

void uart1_send(INT8U str)
{
	while (!(Uart1_LSR & 0x20));
    Uart1_TxData = str;
}

