#define GLOBAL

#include"Main.h"

void delay_us(INT32U x);		//us单位 
void delay_ms(INT32U x);		//ms单位 
void delay_s(INT32U x);  	    //s 单位

int main(void)
{
	PMU_WdtCfg = 0x80007fff;          //看门够设置最大时间
	
	while(1)
	{
		gpio_write_pin(GPIO_PIN_1,GPIO_LOW);   //蓝
		gpio_write_pin(GPIO_PIN_2,GPIO_HIGH);  //LED灭
		gpio_write_pin(GPIO_PIN_3,GPIO_HIGH);  //LED亮
		delay_ms(200);
		gpio_write_pin(GPIO_PIN_1,GPIO_HIGH);  //LED灭
		gpio_write_pin(GPIO_PIN_2,GPIO_LOW);   //红
		gpio_write_pin(GPIO_PIN_3,GPIO_HIGH);  //LED灭
		delay_ms(200);
		gpio_write_pin(GPIO_PIN_1,GPIO_HIGH);  //LED灭
		gpio_write_pin(GPIO_PIN_2,GPIO_HIGH);  //LED灭
		gpio_write_pin(GPIO_PIN_3,GPIO_LOW);   //绿
		delay_ms(200);
	}

	return 0;
}

void delay_us(INT32U x)	     //粗略us延时,不准确
{
	volatile INT32U ii = 0;
	for(ii=0;ii<x;ii++);
}

void delay_ms(INT32U x)  	//粗略ms延时,不准确
{
	volatile INT32U ii = 0;
	for(ii=0;ii<x;ii++)
		delay_us(520);
}

void delay_s(INT32U x)  	//粗略s延时,不准确 
{
	volatile INT32U ii = 0;
	for(ii=0;ii<x;ii++)
		delay_ms(1000);
}
