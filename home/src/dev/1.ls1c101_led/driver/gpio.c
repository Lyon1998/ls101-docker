#include"Main.h"

/*	龙芯1c101管脚有4种配置模式
 * 	1. GPIO模式
 * 	2. 主功能模式
 * 	3. 第一复用模式
 * 	4. 第二复用模式
 * 	默认为GPIO模式,其中GPIO0,GPIO10,GPIO11,GPIO18,GPIO19,GPIO20,GPI021,GPIO40
 * 	GPIO41、GPIO42、GPIO43、GPIO59、GPIO58、GPIO60、GPIO63在芯片上,没有引出.
 */

//GPIO配置为输入
int gpio_read_pin(INT8U pin)
{   
	if(pin>=GPIO_PIN_MAX)
		return -1;
	PMU_GPIOBit(pin) = 0x00;
    return PMU_GPIOBit(pin);
}

/*
 *  GPIO输出
 *  pin:GPIO管脚
 *  mode: 输出电平高低 
 */
void gpio_write_pin(INT8U pin,INT8U mode)
{	
	if(pin>=GPIO_PIN_MAX)
		return;
		
	if(GPIO_HIGH == mode)
	{
		PMU_GPIOBit(pin) = 0x3;
	}
	else 
	{
		PMU_GPIOBit(pin) = 0x2;
	}
}






