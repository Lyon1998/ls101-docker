DOCKER_NAME=ls1c101

docker rm $DOCKER_NAME -f
docker rmi $DOCKER_NAME

docker build -t $DOCKER_NAME .
docker run \
-it \
--privileged=true \
--name $DOCKER_NAME \
--restart=always \
-v $PWD/../opt:/opt \
-v $PWD/../home:/root \
-v /dev/bus/usb:/dev/bus/usb \
-w /root/src \
$DOCKER_NAME \
bash
# sh init.sh
