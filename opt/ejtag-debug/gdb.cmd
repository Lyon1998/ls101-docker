set architecture mips:isa32
set mips abi o32
mem 0xa0000000 0xa0010000 rw
mem 0xbfc00000 0xbfc10000 ro
mem 0xbf000000 0xbf100000 ro
set remote hardware-watchpoint-limit 1
set remote hardware-breakpoint-limit 1
set remotetimeout 20
define mysi
monitor si 0
c
end
target remote 127.0.0.1:50010
